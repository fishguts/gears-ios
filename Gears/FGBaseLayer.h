//
//  FGBaseLayer.h
//  Gears
//
//  Created by Curtis Elsasser on 3/27/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface FGBaseLayer : CALayer

/**** public/protected interface ****/
- (id)initWithFrame:(CGRect)frame;
- (CGRect)topLevelGearBounds;

@end
