//
//  FGDrawingLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDrawingBitmapLayer.h"
#import "FGDrawingPath.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGGear.h"
#import "FGGearState.h"
#import "FGMachineGeometry.h"
#import "FGFrameCalculator.h"
#import "FGBitmapContext.h"
#import "FGValueWrappers.h"
#import "FGDefaults.h"


@interface FGDrawingBitmapLayer()
{
@private
	NSMutableDictionary *_gearToPathsMap;
	FGBitmapContext *_bitmapContext;
}

@end


@implementation FGDrawingBitmapLayer

/**** public interface ****/
- (id)initWithFrame:(CGRect)frame
{
	self=[super initWithFrame:frame];
	if(self!=nil)
	{
		self->_gearToPathsMap=[NSMutableDictionary dictionary];
		self->_bitmapContext=[[FGBitmapContext alloc] initWithSize:frame.size];
		
		// configure (until we introduce color this only needs to be done once)
		CGContextSetStrokeColorWithColor(self->_bitmapContext.context, [[UIColor blackColor] CGColor]);
	}
	return self;
}

- (void)dealloc
{
	[self clearLayer];
}

- (void)clearLayer
{
	CGContextClearRect(self->_bitmapContext.context, self.bounds);
	[self->_gearToPathsMap removeAllObjects];
	[self setNeedsDisplay];
}

- (void)preMachineAdvance
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;
	
	while(gearState!=nil) 
	{
		const int penCount=[gearState.gear.penCount intValue];
		NSMutableArray *penPoints=[self->_gearToPathsMap objectForKey:gearState.id];
		
		// make sure we've got the path array registered (just starting or new gear).
		if(penPoints==nil)
		{
			penPoints=[NSMutableArray array];
			[self->_gearToPathsMap setObject:penPoints forKey:gearState.id];
		}
		
		// note: must get each gear's bounds to find the next gears bounds.
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		
		// the following block creates inital points and removes no longer referenced pens
		// If we've already got the paths started then we are done for this gear.
		if([penPoints count]!=penCount)
		{
			if([penPoints count]<penCount)
			{
				CGPoint points[penCount];
				[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
				
				// and paths and points
				for(int penIndex=[penPoints count]; penIndex<penCount; penIndex++)
				{
					[penPoints addObject:[[FGPoint alloc] initWithValue:points[penIndex]]];
				}
			}
			else
			{
				// points that have been removed
				while([penPoints count]>penCount)
				{
					[penPoints removeLastObject];
				}
			}
		}
		
		// advance
		gearState=gearState.child;
	}
}

- (void)postMachineAdvance
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;
	
	while(gearState!=nil)
	{
		int penCount=[gearState.gear.penCount intValue];
		
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		if(penCount>0)
		{
			CGPoint points[penCount];
			NSMutableArray *penPoints=[self->_gearToPathsMap objectForKey:gearState.id];
			
			[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
			for(int penIndex=0; penIndex<penCount; penIndex++)
			{
				FGPoint *pointFromWrapper=[penPoints objectAtIndex:penIndex];

				// draw from the previously cached pen point to the current pen point
				CGContextBeginPath(self->_bitmapContext.context);
				CGContextMoveToPoint(self->_bitmapContext.context, pointFromWrapper->value.x, pointFromWrapper->value.y);
				CGContextAddLineToPoint(self->_bitmapContext.context, points[penIndex].x, points[penIndex].y);
				CGContextStrokePath(self->_bitmapContext.context);
				
				// replace from point with current to point for next go around
				pointFromWrapper->value=points[penIndex];
			}
			
		}
		// advance
		gearState=gearState.child;
	}
}

/**** path drawing and management ****/
- (void)drawInContext:(CGContextRef)context
{
	CGImageRef image=[self->_bitmapContext getSnapshot:NO];
	
	CGContextDrawImage(context, self.bounds, image);
	CGImageRelease(image);
}

@end
