//
//  FGTextEditorTableViewCell.m
//  Gears
//
//  Created by Curtis Elsasser on 4/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTextEditorTableViewCell.h"

@implementation FGTextEditorTableViewCell
@synthesize propertyName=_propertyName;
@synthesize propertyValue=_propertyValue;

- (void)setPropertyValue:(UITextField *)propertyValue
{
	self->_propertyValue=propertyValue;
	self->_propertyValue.delegate=self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.propertyValue resignFirstResponder];
	return NO;
}

@end
