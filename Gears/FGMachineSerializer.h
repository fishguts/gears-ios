//
//  FGMachineSerializer.h
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMachine;

@interface FGMachineSerializer : FGStaticClass

/**** public interface: machine <-> JSON ****/
+ (NSString*)machineToJSON:(FGMachine*)machine pretty:(BOOL)pretty;
+ (NSString*)machinesToJSON:(NSArray*)machines pretty:(BOOL)pretty;

// context for the following can be nil.
+ (FGMachine*)jsonToMachine:(NSString*)json addToDB:(BOOL)add;
+ (NSArray*)jsonToMachines:(NSString*)json addToDB:(BOOL)add;

@end
