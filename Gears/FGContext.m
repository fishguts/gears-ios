//
//  FGContext.m
//  Gears
//
//  Created by Curtis Elsasser on 3/27/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGContext.h"

@implementation FGContext

+ (void)showTextAtPoint:(CGPoint)point text:(NSString*)text context:(CGContextRef)context
{
	[self showTextAtPoint:point text:text fontFamily:@"System" fontSize:12.0 rotation:0.0 context:context];
}

/* tood: fix orientation and stuff */
+ (void)showTextAtPoint:(CGPoint)point text:(NSString *)text fontFamily:(NSString*)fontFamily fontSize:(CGFloat)fontSize context:(CGContextRef)context
{
	[self showTextAtPoint:point text:text fontFamily:fontFamily fontSize:fontSize rotation:0.0 context:context];
}

+ (void)showTextAtPoint:(CGPoint)point text:(NSString *)text fontFamily:(NSString*)fontFamily fontSize:(CGFloat)fontSize rotation:(CGFloat)rotation context:(CGContextRef)context
{
	CGContextSaveGState(context);
	{
		// flip the coordinate system and rotate
		CGAffineTransform transformOriginal=CGContextGetTextMatrix(context);
		{
			CGAffineTransform transformModified=transformOriginal;
			transformModified=CGAffineTransformScale(transformModified, 1.0, -1.0);
			if(rotation!=0)
			{
				transformModified=CGAffineTransformRotate(transformModified, rotation);
			}
			CGContextSetTextMatrix(context, transformModified);
			
			CGContextSetTextDrawingMode(context, kCGTextFill); 
			CGContextSelectFont(context, [fontFamily cStringUsingEncoding:NSUTF8StringEncoding], fontSize, kCGEncodingMacRoman);
			CGContextShowTextAtPoint(context, point.x, point.y+fontSize, [text cStringUsingEncoding:NSUTF8StringEncoding], [text length]);
		}
		// restore the untainted transform
		CGContextSetTextMatrix(context, transformOriginal);
	}
	CGContextRestoreGState(context);
}

@end
