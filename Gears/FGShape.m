//
//  FGShape.m
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGShape.h"

static const CGPoint pointsDoubleHeadedArrowHorizontal[]={
	{ 0, 0.5 },
	{ 0.1, 0.42 },
	{ 0.1, 0.47 },
	{ 0.9, 0.47 },
	{ 0.9, 0.42 },
	{ 1.0, 0.5 },
	{ 0.9, 0.58 },
	{ 0.9, 0.53 },
	{ 0.1, 0.53 },
	{ 0.1, 0.58 },
};

static const CGPoint pointsDoubleHeadedArrowVertical[]={
	{ 0.5, 0.0 },
	{ 0.42, 0.1 },
	{ 0.47, 0.1 },
	{ 0.47, 0.9 },
	{ 0.42, 0.9 },
	{ 0.5, 1.0 },
	{ 0.58, 0.9 },
	{ 0.53, 0.9 },
	{ 0.53, 0.1 },
	{ 0.58, 0.1 },
};

@implementation FGShape

+ (void)buildShape:(const CGPoint[])points length:(int)length bounds:(CGRect)bounds context:(CGContextRef)context
{
	CGPoint *absolute=malloc(sizeof(CGPoint)*length);
	for(int index=0; index<length; index++)
	{
		absolute[index].x=bounds.origin.x+(points[index].x*bounds.size.width);
		absolute[index].y=bounds.origin.y+(points[index].y*bounds.size.height);
	}
	CGContextAddLines(context, absolute, length);
	free(absolute);
}


+ (void)buildDoubleHeadedArrowHorizontal:(CGRect)bounds context:(CGContextRef)context
{
	[self buildShape:pointsDoubleHeadedArrowHorizontal length:sizeof(pointsDoubleHeadedArrowHorizontal)/sizeof(CGPoint) bounds:bounds context:context];
}

+ (void)buildDoubleHeadedArrowVertical:(CGRect)bounds context:(CGContextRef)context
{
	[self buildShape:pointsDoubleHeadedArrowVertical length:sizeof(pointsDoubleHeadedArrowHorizontal)/sizeof(CGPoint) bounds:bounds context:context];
}


@end
