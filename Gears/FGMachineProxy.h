//
//  FGMachineProxy.h
//  Gears
//
//  Created by Curtis Elsasser on 3/19/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMachine;
@class FGMachineState;
@class FGGear;
@class FGGearState;


@interface FGMachineProxy : FGStaticClass

/**** public interface: machine ****/
+ (void)setMachine:(FGMachine*)machine title:(NSString*)title;
+ (void)setMachine:(FGMachine*)machine drawingFadeDelay:(NSNumber*)delay;
+ (void)setMachine:(FGMachine*)machine drawingFadeDuration:(NSNumber*)duration;
+ (void)setMachine:(FGMachine*)machine thumbData:(NSData*)data;
+ (void)setMachineState:(FGMachineState*)machineState running:(BOOL)running;
+ (void)setMachineState:(FGMachineState*)machineState timeOffset:(NSTimeInterval)timeOffset;

/**** public interface: gear ****/
+ (void)removeGearState:(FGGearState*)gearState machine:(FGMachine*)machineState;
+ (void)setGearSelected:(FGGearState*)gearState machine:(FGMachineState*)machineState;
+ (void)setGear:(FGGear*)gear rpm:(float)rpm machine:(FGMachine*)machineState;
+ (void)setGear:(FGGear*)gear radiusRatio:(float)radiusRatio machine:(FGMachine*)machineState;
+ (void)setGear:(FGGear*)gear penCount:(int)penCount machine:(FGMachine*)machineState;

@end
