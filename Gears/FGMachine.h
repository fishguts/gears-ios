//
//  FGMachine.h
//  Gears
//
//  Created by Curtis Elsasser on 5/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGGear;

@interface FGMachine : NSManagedObject

@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * modified;
@property (nonatomic, retain) NSData * thumb;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * drawingFadeDuration;
@property (nonatomic, retain) NSNumber * drawingFadeDelay;
@property (nonatomic, retain) FGGear *gearTopLevel;

@end
