//
//  FGEditorDataFactory.m
//  Gears
//
//  Created by Curtis Elsasser on 5/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGEditorDataFactory.h"
#import "FGDefaults.h"
#import "NSString+FGFormat.h"

@implementation FGEditorDataFactory

+ (void)prepPrimitiveArrays:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	if(*primitiveValues==nil)
	{
		*primitiveValues=[NSMutableArray array];
	}
	else
	{
		[*primitiveValues removeAllObjects];
	}
	if(*friendlyValues==nil)
	{
		*friendlyValues=[NSMutableArray array];
	}
	else
	{
		[*friendlyValues removeAllObjects];
	}
}

+ (void)getGearCountPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	for(int index=1; index<=20; index++)
	{
		[*primitiveValues addObject:[NSNumber numberWithInt:index]];
		[*friendlyValues addObject:[NSString formatGearCount:index]];
	}
}

+ (void)getGearSizePrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	for(float index=0.1; index<1.0; index+=0.05)
	{
		[*primitiveValues addObject:[NSNumber numberWithFloat:index]];
		[*friendlyValues addObject:[NSString formatSizeRatio:index]];
	}
}

+ (void)getRPMPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	for(float index=-100; index<=100; index+=1)
	{
		[*primitiveValues addObject:[NSNumber numberWithFloat:index]];
		[*friendlyValues addObject:[NSString formatRPM:index]];
	}
}

+ (void)getRPMRatioPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	for(float index=-5; index<=5; index+=0.25)
	{
		[*primitiveValues addObject:[NSNumber numberWithFloat:index]];
		[*friendlyValues addObject:[NSString formatRPMRatio:index]];
	}
}

+ (void)getFadeDelayPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	[*primitiveValues addObject:[NSNumber numberWithFloat:FLT_MIN]];
	[*friendlyValues addObject:[NSString formatFadeDelay:FLT_MIN]];
	for(float index=0; index<=60; index+=1)
	{
		[*primitiveValues addObject:[NSNumber numberWithFloat:index]];
		[*friendlyValues addObject:[NSString formatFadeDelay:index]];
	}
}

+ (void)getFadeDurationPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues
{
	[FGEditorDataFactory prepPrimitiveArrays:primitiveValues friendlyValues:friendlyValues];
	[*primitiveValues addObject:[NSNumber numberWithFloat:FLT_MIN]];
	[*friendlyValues addObject:[NSString formatFadeDuration:FLT_MIN]];
	for(float index=1; index<=10.0; index+=1)
	{
		[*primitiveValues addObject:[NSNumber numberWithFloat:index]];
		[*friendlyValues addObject:[NSString formatFadeDuration:index]];
	}
}

@end
