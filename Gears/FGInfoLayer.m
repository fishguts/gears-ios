//
//  FGInfoLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGInfoLayer.h"
#import "FGGear.h"
#import "FGContext.h"
#import "FGShape.h"
#import "FGDefaults.h"


@interface FGInfoLayer()
/**** private methods ****/
- (void)drawRPMInContext:(CGContextRef)context;
- (void)drawPenCountInContext:(CGContextRef)context;
- (void)drawBothInContext:(CGContextRef)context;
@end


@implementation FGInfoLayer
@synthesize gear=_gear;


/**** properties ****/
@synthesize overlayState=_overlayType;

- (void)setOverlayState:(FGInfoOverlayType)overlayType
{
	if(overlayType!=self->_overlayType)
	{
		self->_overlayType=overlayType;
		[self setNeedsDisplay];
	}
}

- (void)setGear:(FGGear *)gear
{
	if(self->_gear!=gear)
	{
		self->_gear=gear;
		[self setNeedsDisplay];
	}
}

/**** drawing ****/
- (void)drawRPMInContext:(CGContextRef)context
{
	// draw vertical arrow
	CGContextSetFillColorWithColor(context, [[FGDefaults getOverlayColorFill] CGColor]);
	[FGShape buildDoubleHeadedArrowVertical:self.bounds context:context];
	CGContextFillPath(context);
	
	// draw text
	NSString *text=[NSString stringWithFormat:@"RPM %.0f", [self.gear.rpm floatValue]];
	CGContextSetFillColorWithColor(context, [[FGDefaults getOverlayColorText] CGColor]);
	[FGContext showTextAtPoint:CGPointMake(CGRectGetMidX(self.bounds)-7, 14) text:text fontFamily:FGDefaultOverlayFontFamily fontSize:18 rotation:0-M_PI_2 context:context];
}

- (void)drawPenCountInContext:(CGContextRef)context
{
	// draw horizontal arrow
	CGContextSetFillColorWithColor(context, [[FGDefaults getOverlayColorFill] CGColor]);
	[FGShape buildDoubleHeadedArrowHorizontal:self.bounds context:context];
	CGContextFillPath(context);

	// draw text
	NSString *text=[NSString stringWithFormat:@"PEN COUNT %d", [self.gear.penCount intValue]];
	CGContextSetFillColorWithColor(context, [[FGDefaults getOverlayColorText] CGColor]);
	[FGContext showTextAtPoint:CGPointMake(32, CGRectGetMidY(self.bounds)-11) text:text fontFamily:FGDefaultOverlayFontFamily fontSize:18 context:context];
}

- (void)drawBothInContext:(CGContextRef)context
{
	[self drawRPMInContext:context];
	[self drawPenCountInContext:context];
}

- (void)drawInContext:(CGContextRef)context
{
	switch(self.overlayState)
	{
		case FGInfoOverlayRPM:
		{
			[self drawRPMInContext:context];
			break;
		}
		case FGInfoOverlayPenCount:
		{
			[self drawPenCountInContext:context];
			break;
		}
		case FGInfoOverlayRPMandPenCount:
		{
			[self drawBothInContext:context];
			break;
		}
		default:
		{
			NSAssert((self.overlayState==FGInfoOverlayNone), @"Unknown type");
			break;
		}
	}
}

@end
