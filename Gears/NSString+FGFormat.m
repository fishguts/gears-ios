//
//  NSString+FGFormat.m
//  Gears
//
//  Created by Curtis Elsasser on 5/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSString+FGFormat.h"

@implementation NSString (FGFormat)

+ (NSString*)formatGearCount:(int)value
{
	return [NSString stringWithFormat:@"%d", value];
}
+ (NSString*)formatSizeRatio:(float)value
{
	return [NSString stringWithFormat:@"%.2f x Parent", value];
}
+ (NSString*)formatRPM:(float)value
{
	return [NSString stringWithFormat:@"%.0f", value];
}
+ (NSString*)formatRPMRatio:(float)value
{
	return [NSString stringWithFormat:@"%.2f x Parent", value];
}
+ (NSString*)formatFadeDelay:(float)value
{
	return (value!=FLT_MIN) ? [NSString stringWithFormat:@"%.0f Seconds", value] : @"None";
}
+ (NSString*)formatFadeDuration:(float)value
{
	return (value!=FLT_MIN) ? [NSString stringWithFormat:@"%.0f Seconds", value] : @"None";
}

@end
