//
//  FGGearState.h
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGGear;
@class FGGearState;

@interface FGGearState : NSObject

/**** properties ****/
@property (nonatomic, readonly, weak) FGGear *gear;
@property (nonatomic, strong) FGGearState *child;
@property (nonatomic, strong) FGGearState *parent;
@property (nonatomic, readonly, strong) NSString *id;
// rotation is in terms of revolutions (not radians)
@property (nonatomic) float rotationOffset;
@property (nonatomic) BOOL selected;

/**** instance methods ****/
- (id)initWithGear:(FGGear*)gear parent:(FGGearState*)parent;
- (void)remove;

@end
