//
//  FGTypes.h
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//
#ifndef __FGTYPES_H
#define __FGTYPES_H

@class FGMachine;

/**** types ****/
typedef enum 
{
	FGInfoOverlayNone=0,
	FGInfoOverlayRPM=0x1<<0,
	FGInfoOverlayPenCount=0x1<<1,
	FGInfoOverlayRPMandPenCount=(FGInfoOverlayRPM|FGInfoOverlayPenCount),
	
} FGInfoOverlayType;


/**** protocols ****/
@protocol FGDispose <NSObject>
- (void)dispose;
@end

@protocol FGModalDelegate <NSObject>
- (void)modalCanceled:(id)modal;
- (void)modalComplete:(id)modal;
@end

@protocol FGEditorDelegate <NSObject>
- (void)editorComplete:(id)editor;
@end

#endif
