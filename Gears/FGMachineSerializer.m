//
//  FGMachineSerializer.m
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineSerializer.h"
#import "FGModel.h"
#import "FGMachine.h"
#import "FGGear.h"
#import "FGSerializer.h"
#import "NSNotification+FG.h"


@interface FGMachineSerializer()
/**** private workers *****/
+ (NSDictionary*)machineToDictionary:(FGMachine*)machine;
+ (NSDictionary*)gearToDictionary:(FGGear*)gear;

+ (FGMachine*)dictionaryToMachine:(NSDictionary*)data addToDB:(BOOL)add;
+ (FGGear*)dictionaryToGear:(NSDictionary*)data addToDB:(BOOL)add;

@end

@implementation FGMachineSerializer

+ (NSString*)machineToJSON:(FGMachine*)machine pretty:(BOOL)pretty
{
	return [FGMachineSerializer machinesToJSON:[NSArray arrayWithObject:machine]  pretty:pretty];
}

+ (NSString*)machinesToJSON:(NSArray*)machines pretty:(BOOL)pretty
{
	NSString *jsonText;
	@try 
	{
		NSMutableArray *data=[NSMutableArray array];
		for(FGMachine *machine in machines)
		{
			[data addObject:[FGMachineSerializer machineToDictionary:machine]];
		}
		
		NSData *jsonData=[FGSerializer dataToJSON:data pretty:pretty];
		jsonText=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	}
	@catch(NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed to convert machines to JSON: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		// rethrow so that the folks back home know what happened
		@throw exception;
	}
	
	return jsonText;
}

+ (FGMachine*)jsonToMachine:(NSString*)json addToDB:(BOOL)add
{
	NSArray *array=[FGMachineSerializer jsonToMachines:json addToDB:add];
	return [array objectAtIndex:0];
}

+ (NSArray*)jsonToMachines:(NSString*)json addToDB:(BOOL)add
{
	NSMutableArray *results=[NSMutableArray array];
	@try 
	{
		NSArray *machinesData=[FGSerializer jsonToData:[json dataUsingEncoding:NSUTF8StringEncoding]];
		
		for(NSDictionary *machineData in machinesData)
		{
			[results addObject:[FGMachineSerializer dictionaryToMachine:machineData addToDB:add]];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *errorText=[NSString stringWithFormat:@"Failed convert JSON to machines: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:errorText]];
		// rethrow so that the folks back home know what happened
		@throw exception;
	}
	
	return results;
}

#pragma mark - Private Interface
+ (NSDictionary*)machineToDictionary:(FGMachine*)machine
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	
	[data setObject:machine.title forKey:@"title"];
	if(machine.author!=nil)
	{
		[data setObject:machine.author forKey:@"author"];
	}
	if(machine.drawingFadeDuration!=nil)
	{
		[data setObject:machine.drawingFadeDuration forKey:@"drawingFadeDuration"];
	}
	if(machine.drawingFadeDelay!=nil)
	{
		[data setObject:machine.drawingFadeDelay forKey:@"drawingFadeDelay"];
	}
	[data setObject:[FGSerializer dateToString:machine.created] forKey:@"created"];
	[data setObject:[FGSerializer dateToString:machine.modified] forKey:@"modified"];
	[data setObject:[FGMachineSerializer gearToDictionary:machine.gearTopLevel] forKey:@"gearTopLevel"];

	return data;
}

+ (NSDictionary*)gearToDictionary:(FGGear*)gear
{
	NSMutableDictionary *data=[NSMutableDictionary dictionary];
	
	[data setObject:gear.penCount forKey:@"penCount"];
	[data setObject:gear.radiusRatio forKey:@"radiusRatio"];
	[data setObject:gear.rpm forKey:@"rpm"];
	if(gear.child!=nil)
	{
		[data setObject:[FGMachineSerializer gearToDictionary:gear.child] forKey:@"child"];
	}
	
	return data;
}

+ (FGMachine*)dictionaryToMachine:(NSDictionary*)data addToDB:(BOOL)add
{
	NSManagedObjectContext *context=[[FGModel instance] context];
	NSEntityDescription *entity=[NSEntityDescription entityForName:@"FGMachine" inManagedObjectContext:context];
	// note: nil is supposed to prevent it from being added but it's not working?
	FGMachine *machine=[[FGMachine alloc] initWithEntity:entity insertIntoManagedObjectContext:(add) ? context : nil];
	
	machine.title=[data valueForKey:@"title"];
	machine.author=[data valueForKey:@"author"];
	machine.drawingFadeDuration=[data valueForKey:@"drawingFadeDuration"];
	machine.drawingFadeDelay=[data valueForKey:@"drawingFadeDelay"];
	machine.created=[FGSerializer stringToDate:[data valueForKey:@"created"]];
	machine.modified=[FGSerializer stringToDate:[data valueForKey:@"modified"]];
	machine.gearTopLevel=[FGMachineSerializer dictionaryToGear:[data valueForKey:@"gearTopLevel"] addToDB:add];

	return machine;
}

+ (FGGear*)dictionaryToGear:(NSDictionary*)data addToDB:(BOOL)add
{
	FGGear *gear;
	
	if(data!=nil)
	{
		NSManagedObjectContext *context=[[FGModel instance] context];
		NSEntityDescription *entity=[NSEntityDescription entityForName:@"FGGear" inManagedObjectContext:context];
		gear=[[FGGear alloc] initWithEntity:entity insertIntoManagedObjectContext:((add) ? context : nil)];
	
		gear.penCount=[data valueForKey:@"penCount"];
		gear.radiusRatio=[data valueForKey:@"radiusRatio"];
		gear.rpm=[data valueForKey:@"rpm"];
		gear.child=[FGMachineSerializer dictionaryToGear:[data valueForKey:@"child"] addToDB:add];
		if(gear.child!=nil)
		{
			gear.child.parent=gear;
		}
	}
	return gear;
}


@end
