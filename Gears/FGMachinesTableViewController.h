//
//  FGMachineTableViewController.h
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "FGTypes.h"


@interface FGMachinesTableViewController : UITableViewController <NSFetchedResultsControllerDelegate, FGModalDelegate>

@end
