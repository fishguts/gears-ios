//
//  FGMachineState.m
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineState.h"
#import "FGMachine.h"
#import "FGGearState.h"
#import "FGGear.h"


@implementation FGMachineState
/**** properties ****/
@synthesize machine=_machine;
@synthesize topLevelGearState=_topLevelGearState;
@synthesize timeOffset=_timeOffset;
@synthesize running=_running;

/**** instance methods ****/
- (id)initWithMachine:(FGMachine*)machine
{
	self=[super init];
	self->_machine=machine;
	
	// setup gears
	FGGear *gear=machine.gearTopLevel;
	FGGearState *gearState=self->_topLevelGearState=[[FGGearState alloc] initWithGear:gear parent:nil];
	while((gear=gear.child)!=nil)
	{
		gearState=[[FGGearState alloc] initWithGear:gear parent:gearState];
	}
	
	return self;
}

- (FGGearState*)getSelectedGearState
{
	FGGearState *gearState=self.topLevelGearState;
	while((gearState!=nil) && (gearState.selected==NO))
	{
		gearState=gearState.child;
	}
	return gearState;
}

- (void)setSelectedGearState:(FGGearState *)selectedGearState
{
	// only allow one selection at any one time
	for(FGGearState *gearState=self.topLevelGearState; gearState!=nil; gearState=gearState.child)
	{
		gearState.selected=NO;
	}
	selectedGearState.selected=YES;
}



@end
