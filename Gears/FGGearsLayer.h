//
//  FGearsLayer.h
//  Gears
//
//  Created by Curtis Elsasser on 3/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBaseLayer.h"

@class FGMachineState;
@class FGGearState;

@interface FGGearsLayer : FGBaseLayer

/**** properties ****/
@property (nonatomic, weak) FGMachineState *machineState;

/**** public interface ****/
- (FGGearState*)gearStateFromPoint:(CGPoint)point;
- (FGGearState*)gearStateFromPoint:(CGPoint)point withRadius:(CGFloat)radius;

@end
