//
//  Notifications.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGNotifications.h"

/**** error notifications ****/
NSString *const NotificationDebug=@"FGNotificationDebug";
NSString *const NotificationInfo=@"FGNotificationInfo";
NSString *const NotificationWarn=@"FGNotificationWarn";
NSString *const NotificationError=@"FGNotificationError";

/**** document notifications ****/
NSString *const NotificationDBOpening=@"FGNotificationDBOpening";
NSString *const NotificationDBOpenSucceeded=@"FGNotificationDBOpenSucceded";
NSString *const NotificationDBOpenFailed=@"FGNotificationDBOpenFailed";
NSString *const NotificationDBSaving=@"FGNotificationDBSaving";
NSString *const NotificationDBSaveSucceeded=@"FGNotificationDBSaveSucceeded";
NSString *const NotificationDBSaveFailed=@"FGNotificationDBSaveFailed";

/**** model and state modification notifications ****/
NSString *const NotificationDBModified=@"FGNotificationDBModified";

NSString *const NotificationRunningStateChanged=@"FGNotificationRunningStateChanged";
NSString *const NotificationMachineStateChanged=@"FGNotificationMachineStateChanged";
NSString *const NotificationSelectionStateChanged=@"FGNotificationSelectionStateChanged";
