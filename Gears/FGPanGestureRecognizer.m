//
//  FGPanGestureRecognizer.m
//  Junk
//
//  Created by Curtis Elsasser on 3/25/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGPanGestureRecognizer.h"
#import "FGDefaults.h"
#import <UIKit/UIGestureRecognizerSubclass.h>


@implementation FGPanGestureRecognizer
/**** properties ****/
@synthesize direction=_direction;


- (void)reset
{
	self->_locationBegan=CGPointZero;
	self->_direction=FGPanDirectionUnknown;
}

- (CGPoint)locationInView:(UIView *)view
{
	CGPoint result=[super locationInView:view];
	if(self.direction==FGPanDirectionHorizontal)
	{
		result.y=self->_locationBegan.y;
	}
	else if(self.direction==FGPanDirectionVertical)
	{
		result.x=self->_locationBegan.x;
	}
	return result;
}

- (CGPoint)translationInView:(UIView *)view
{
	CGPoint result=[super translationInView:view];
	if(self.direction==FGPanDirectionHorizontal)
	{
		result.y=0;
	}
	else if(self.direction==FGPanDirectionVertical)
	{
		result.x=0;
	}
	return result;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch=[[touches allObjects] objectAtIndex:0];

	[super touchesBegan:touches withEvent:event];
	self->_locationBegan=[touch locationInView:self.view];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(self.state==UIGestureRecognizerStatePossible)
	{
		UITouch *touch=[[touches allObjects] objectAtIndex:0];
		CGPoint location=[touch locationInView:self.view];
		CGFloat deltaX=fabsf(location.x-self->_locationBegan.x);
		CGFloat deltaY=fabsf(location.y-self->_locationBegan.y);
		
		if(deltaX>deltaY)
		{
			if(deltaX>FGDefaultPanThreshold)
			{
				self->_direction=FGPanDirectionHorizontal;
				// move state from possible to begain
				[super touchesMoved:touches withEvent:event];
			}
		}
		else
		{
			if(deltaY>FGDefaultPanThreshold)
			{
				self->_direction=FGPanDirectionVertical;
				// move state from possible to begain
				[super touchesMoved:touches withEvent:event];
			}
		}
	}
	else
	{
		[super touchesMoved:touches withEvent:event];
	}
}


@end
