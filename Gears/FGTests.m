//
//  FGTest.m
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGTests.h"
#import "FGMachine.h"
#import "FGMachineFactory.h"
#import "FGMachineSerializer.h"

@interface FGTests()
#if DEBUG
	+ (void)runSerializeTest;
#endif
@end

@implementation FGTests

+ (void)runSimple
{
#if DEBUG
#endif
}

+ (void)runDB
{
#if DEBUG && 0
	[FGTests runSerializeTest];
#endif
}


#if DEBUG
+ (void)runSerializeTest
{
	NSString *json;
	FGMachine *machine;
	FGGear *gear;

	machine=[FGMachineFactory createMachineWithTitle:@"Serialize Test"];
	gear=[FGMachineFactory createGearWithParent:machine.gearTopLevel machine:machine];
	gear=[FGMachineFactory createGearWithParent:gear machine:machine];
	gear=[FGMachineFactory createGearWithParent:gear machine:machine];
	json=[FGMachineSerializer machineToJSON:machine pretty:YES];
	
	NSLog(@"Machine json:\n%@", json);
	
	machine=[FGMachineSerializer jsonToMachine:json addToDB:YES];
}

#endif
@end
