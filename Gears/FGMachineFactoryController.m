//
//  FGMachineFactoryController.m
//  Gears
//
//  Created by Curtis Elsasser on 5/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineFactoryController.h"
#import "FGEditorPickerController.h"
#import "FGEditorDataFactory.h"
#import "FGMachineGeometry.h"
#import "FGMachine.h"
#import "FGGear.h"
#import "FGGear+Math.h"
#import "FGMachineFactory.h"
#import "FGDefaults.h"
#import "NSString+FGFormat.h"

@interface FGMachineFactoryController()
{
@private
	int _gearCount;
	float _gearRPM;
	float _sizeRatio;
	float _rpmRatio;
	float _fadeDelay;
	float _fadeDuration;
	
	NSMutableArray *_editingFriendlyValues;
	NSMutableArray *_editingDataValues;
}

@property (weak, nonatomic) IBOutlet UITextField *textTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelGearCount;
@property (weak, nonatomic) IBOutlet UILabel *labelRPM;
@property (weak, nonatomic) IBOutlet UILabel *labelSizeRatio;
@property (weak, nonatomic) IBOutlet UILabel *labelRPMRatio;
@property (weak, nonatomic) IBOutlet UILabel *labelFadeDelay;
@property (weak, nonatomic) IBOutlet UILabel *labelFadeDuration;

/** workers **/
- (void)dataToUI;

@end


@implementation FGMachineFactoryController
@synthesize delegate;
@synthesize targetBounds;
@synthesize labelGearCount;
@synthesize labelRPM;
@synthesize labelSizeRatio;
@synthesize labelRPMRatio;
@synthesize labelFadeDelay;
@synthesize labelFadeDuration;
@synthesize textTitle;

#pragma mark - workers
- (void)dataToUI
{
	self.labelGearCount.text=[NSString formatGearCount:self->_gearCount];
	self.labelSizeRatio.text=[NSString formatSizeRatio:self->_sizeRatio];
	self.labelRPM.text=[NSString formatRPM:self->_gearRPM];
	self.labelRPMRatio.text=[NSString formatRPMRatio:self->_rpmRatio];
	self.labelFadeDelay.text=[NSString formatFadeDelay:self->_fadeDelay];
	self.labelFadeDuration.text=[NSString formatFadeDuration:self->_fadeDuration];
}

- (FGMachine*)machineFromUI
{
	FGMachine *machine=[FGMachineFactory createMachineWithTitle:self.textTitle.text];
	FGGear *gearParent=machine.gearTopLevel;
	float size=self->_sizeRatio;
	float rpm=self->_gearRPM;
	
	// configure machine
	machine.drawingFadeDelay=(self->_fadeDelay!=FLT_MIN) ? [NSNumber numberWithFloat:self->_fadeDelay] : nil;
	machine.drawingFadeDuration=(self->_fadeDuration!=FLT_MIN) ? [NSNumber numberWithFloat:self->_fadeDuration] : nil;
	
	// configure gears
	for(int indexGear=1; indexGear<self->_gearCount; indexGear++)
	{
		if([FGMachineGeometry canGearContainChild:gearParent totalBounds:self.targetBounds]==NO)
		{
			// that's all she can take, she can't takes no mores
			break;
		}
		else
		{
			const float sizeMin=[gearParent childRadiusRatioMinWithTotalBounds:self.targetBounds.size];
			const float sizeMax=[gearParent childRadiusRatioMaxWithTotalBounds:self.targetBounds.size];								 
			FGGear *gearChild=[FGMachineFactory createGearWithParent:gearParent radiusRatio:MAX(sizeMin, MIN(sizeMax, size)) penCount:FGDefaultGearPenCount rpm:rpm machine:machine];

			// prep for next round
			gearParent=gearChild;
			size*=self->_sizeRatio;
			rpm*=self->_rpmRatio;
		}
	}
	
	return machine;
}

#pragma mark - overrides
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	FGEditorPickerController *pickerController;
	
	[self.textTitle resignFirstResponder];
	if([segue.identifier isEqualToString:@"Count Segue"])
	{
		[FGEditorDataFactory getGearCountPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Gears";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=66.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatGearCount:self->_gearCount];
	}
	else if([segue.identifier isEqualToString:@"Size Segue"])
	{
		[FGEditorDataFactory getGearSizePrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Size Ratio";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=160.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatSizeRatio:self->_sizeRatio];
	}
	else if([segue.identifier isEqualToString:@"RPM Segue"])
	{
		[FGEditorDataFactory getRPMPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"RPM";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=66.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatRPM:self->_gearRPM];
	}
	else if([segue.identifier isEqualToString:@"RPM Ratio Segue"])
	{
		[FGEditorDataFactory getRPMRatioPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"RPM Ratio";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=160.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatRPMRatio:self->_rpmRatio];
	}
	else if([segue.identifier isEqualToString:@"Fade Delay Segue"])
	{
		[FGEditorDataFactory getFadeDelayPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Fade Delay";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=154.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatFadeDelay:self->_fadeDelay];
	}
	else if([segue.identifier isEqualToString:@"Fade Duration Segue"])
	{
		[FGEditorDataFactory getFadeDurationPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Fade Duration";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=154.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatFadeDuration:self->_fadeDuration];
	}
}

#pragma mark - FGEditorDelegate 
- (void)editorComplete:(id)editor
{
	NSString *identifier=[editor identifier];
	if([identifier isEqualToString:@"Count Segue"])
	{
		self->_gearCount=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] intValue];
	}
	else if([identifier isEqualToString:@"Size Segue"])
	{
		self->_sizeRatio=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	else if([identifier isEqualToString:@"RPM Segue"])
	{
		self->_gearRPM=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	else if([identifier isEqualToString:@"RPM Ratio Segue"])
	{
		self->_rpmRatio=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	else if([identifier isEqualToString:@"Fade Delay Segue"])
	{
		self->_fadeDelay=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	else if([identifier isEqualToString:@"Fade Duration Segue"])
	{
		self->_fadeDuration=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	[self dataToUI];

	// clean up
	self->_editingFriendlyValues=nil;
	self->_editingDataValues=nil;
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	
    self.clearsSelectionOnViewWillAppear = NO;
	self.textTitle.delegate=self;
	
	// establish defaults
	self->_gearCount=2;
	self->_gearRPM=FGDefaultGearRPM;
	self->_sizeRatio=0.5;
	self->_rpmRatio=1.0;
	self->_fadeDelay=FLT_MIN;
	self->_fadeDuration=FLT_MIN;
	
	[self dataToUI];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.textTitle resignFirstResponder];
	return NO;
}


#pragma mark - observers and actions
- (IBAction)handleCancel:(id)sender 
{
	[self.delegate modalCanceled:self];
}


- (IBAction)handleCreate:(id)sender 
{
	[self.delegate modalComplete:self];
}

@end
