//
//  FGBitmapContext.m
//  BitmapCaching
//
//  Created by Curtis Elsasser on 6/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBitmapContext.h"

@implementation FGBitmapContext

#pragma mark - Public Interface
- (CGContextRef)context
{
	return self->_context;
}

- (id)initWithSize:(CGSize)size
{
	self=[super init];
	
	const float scale=[[UIScreen mainScreen] scale];
	self->_bitmapSize=CGSizeMake(size.width*scale, size.height*scale);
	self->_bitsPerComponent=8;
	self->_bitsPerPixel=self->_bitsPerComponent*4;
	self->_bytesPerRow=(self->_bitmapSize.width*self->_bitsPerPixel);
	// row increments of 16 perform better (as long as bitsPerPixel=32 we are already good)
	if(self->_bytesPerRow%16)
	{
		self->_bytesPerRow+=16-(self->_bytesPerRow%16);
	}
	self->_buffer=calloc(self->_bitmapSize.height*self->_bytesPerRow, 1);
	self->_provider=CGDataProviderCreateWithData(nil, self->_buffer, self->_bitmapSize.height*self->_bytesPerRow, nil);
	self->_colorSpace=CGColorSpaceCreateDeviceRGB();
	self->_bitmapInfo=kCGImageAlphaPremultipliedLast;
	
    self->_context=CGBitmapContextCreate(self->_buffer, self->_bitmapSize.width, self->_bitmapSize.height, self->_bitsPerComponent, self->_bytesPerRow, self->_colorSpace, self->_bitmapInfo);
	CGContextScaleCTM(self->_context, scale, scale);

	return self;
}

- (void)dealloc
{
	[self dispose];
}

- (void)dispose
{
	if(self->_context!=nil)
	{
		CGContextRelease(self->_context);
		self->_context=nil;
	}
	if(self->_colorSpace!=nil)
	{
		CGColorSpaceRelease(self->_colorSpace);
		self->_colorSpace=nil;
	}
	if(self->_provider!=nil)
	{
		CGDataProviderRelease(self->_provider);
		self->_provider=nil;
	}
	if(self->_buffer!=nil)
	{
		free(self->_buffer);
		self->_buffer=nil;
	}
}

- (CGImageRef)getSnapshot:(BOOL)deepCopy
{
	if(deepCopy)
	{
		return CGBitmapContextCreateImage(self->_context);
	}
	else
	{
		return CGImageCreate(self->_bitmapSize.width, self->_bitmapSize.height, self->_bitsPerComponent, self->_bitsPerPixel, self->_bytesPerRow, self->_colorSpace, self->_bitmapInfo, self->_provider, NULL, NO, kCGRenderingIntentDefault);
	}
}

@end
