//
//  FGGear.m
//  Gears
//
//  Created by Curtis Elsasser on 3/19/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGear.h"
#import "FGGear.h"


@implementation FGGear

@dynamic penCount;
@dynamic radiusRatio;
@dynamic rpm;
@dynamic child;
@dynamic parent;

@end
