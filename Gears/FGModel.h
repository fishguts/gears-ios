//
//  FGModel.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGModel : NSObject

/**** singleton ****/
+ (FGModel*)instance;

/**** properties ****/
@property (nonatomic, readonly) NSManagedObjectContext *context;

/**** file IO: note - load is up to each functional unit ****/
- (void)open;
- (void)save;

@end
