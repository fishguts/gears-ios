//
//  FGMachineFactory.h
//  Gears
//
//  Created by Curtis Elsasser on 3/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGMachine;
@class FGMachineState;
@class FGGear;
@class FGGearState;


/* Factory for DB entities */
@interface FGMachineFactory : FGStaticClass

/**** public interface: machine ****/
+ (FGMachine*)createMachine;
+ (FGMachine*)createMachineWithTitle:(NSString*)title;
+ (void)deleteMachine:(FGMachine*)machine;
+ (FGMachineState*)createMachineState:(FGMachine*)machine;

/**** public interface: gears ****/
+ (FGGear*)createGearWithParent:(FGGear*)parent machine:(FGMachine*)machine;
+ (FGGear*)createGearWithParent:(FGGear*)parent radiusRatio:(float)radiusRatio penCount:(int)penCount rpm:(float)rpm machine:(FGMachine*)machine;
+ (FGGearState*)createGearStateWithParent:(FGGearState*)parent machine:(FGMachineState*)machineState;

/**** public interface: presets - should there ever be a centralized repository then this guy should be pulled *****/
+ (void)addPresets;

@end
