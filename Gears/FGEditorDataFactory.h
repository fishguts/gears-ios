//
//  FGEditorDataFactory.h
//  Gears
//
//  Created by Curtis Elsasser on 5/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@interface FGEditorDataFactory : FGStaticClass

+ (void)getGearCountPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;
+ (void)getGearSizePrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;
+ (void)getRPMPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;
+ (void)getRPMRatioPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;
+ (void)getFadeDelayPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;
+ (void)getFadeDurationPrimitiveValues:(NSMutableArray*__strong*)primitiveValues friendlyValues:(NSMutableArray*__strong*)friendlyValues;

@end
