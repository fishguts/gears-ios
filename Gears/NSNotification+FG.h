//
//  NSNotification+FGFactory.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNotification (FGAlert)
+ (id)debugNotificationWithObject:(id)object text:(NSString*)text;
+ (id)infoNotificationWithObject:(id)object text:(NSString*)text;
+ (id)warnNotificationWithObject:(id)object text:(NSString*)text;
+ (id)errorNotificationWithObject:(id)object text:(NSString*)text;

- (NSString*)alertText;
@end

@interface NSNotification (FGDatabase)
+ (id)databaseModifiedWithObject:(id)object autoSave:(BOOL)autoSave;

- (BOOL)isDatabaseAutoSave;
@end