//
//  FGDrawingLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDrawingLayer.h"


@implementation FGDrawingLayer
@synthesize machineState;

/**** public interface ****/
- (void)clearLayer
{
	NSAssert(NO, @"Abstract");
}

- (void)preMachineAdvance
{
	NSAssert(NO, @"Abstract");
}

- (void)postMachineAdvance
{
	NSAssert(NO, @"Abstract");
}

@end
