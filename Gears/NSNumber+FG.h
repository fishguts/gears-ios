//
//  NSNumber+FG.h
//  Gears
//
//  Created by Curtis Elsasser on 5/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (FG)

+ (BOOL)isEqualNumber:(NSNumber*)n1 toNumber:(NSNumber*)n2;
@end
