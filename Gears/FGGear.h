//
//  FGGear.h
//  Gears
//
//  Created by Curtis Elsasser on 3/19/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FGGear;

@interface FGGear : NSManagedObject

@property (nonatomic, retain) NSNumber * penCount;
@property (nonatomic, retain) NSNumber * radiusRatio;
@property (nonatomic, retain) NSNumber * rpm;
@property (nonatomic, retain) FGGear *child;
@property (nonatomic, retain) FGGear *parent;

@end
