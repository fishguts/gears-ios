//
//  FGDrawingPath.h
//  Gears
//
//  Created by Curtis Elsasser on 5/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGDrawingPath : NSObject

/** properties **/
@property (nonatomic) int frameStart;
@property (nonatomic) int frameLastPoint;

/** public interface **/
- (id)initWithFrameStart:(int)frame;

- (void)addPoint:(CGPoint)point frame:(int)frame;
- (int)getPointCount;
- (CGPoint)getPointAt:(int)index;
- (CGPoint*)getPointBufferAt:(int)index;
- (void)removePointsFromBeginning:(int)count;


@end
