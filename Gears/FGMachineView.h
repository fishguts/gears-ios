//
//  FGMachineView.h
//  Gears
//
//  Created by Curtis Elsasser on 3/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@class FGMachineState;
@class FGGearState;

@interface FGMachineView : UIView
/**** properties ****/
@property (nonatomic, weak) FGMachineState *machineState;
@property (nonatomic, readonly) CGRect topLevelGearBounds;

/**** public interface: query ****/
- (FGGearState*)gearStateFromPoint:(CGPoint)point;
- (FGGearState*)gearStateFromPoint:(CGPoint)point withRadius:(CGFloat)radius;

/**** public interface: action ****/
- (void)clearDrawing;
- (void)validateDrawingMethod;

- (void)preMachineAdvance;
- (void)postMachineAdvance;
- (void)redrawMachineState;

- (UIImage*)imageFromGears:(CGSize)size;
- (UIImage*)imageFromDrawing:(CGSize)size;

/**** public interface: info layer ****/
- (void)showInfoLayerWithGear:(FGGearState*)gearState infoType:(FGInfoOverlayType)type pulse:(BOOL)pulse;
- (void)updateInfoLayer;
- (void)hideInfoLayer:(BOOL)animate;

- (void)showGearLayer:(BOOL)animate;
- (void)hideGearLayer:(BOOL)animate;

@end
