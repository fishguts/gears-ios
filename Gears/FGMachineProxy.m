//
//  FGMachineProxy.m
//  Gears
//
//  Created by Curtis Elsasser on 3/19/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineProxy.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGGear.h"
#import "FGGearState.h"
#import "FGNotifications.h"
#import "NSNumber+FG.h"


@implementation FGMachineProxy

/**** machine interface ****/
+ (void)setMachine:(FGMachine*)machine title:(NSString*)title
{
	if(machine.title!=title)
	{
		machine.title=title;
		machine.modified=[NSDate date];
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:machine];
	}
}

+ (void)setMachine:(FGMachine*)machine drawingFadeDelay:(NSNumber*)delay
{
	if([NSNumber isEqualNumber:machine.drawingFadeDelay toNumber:delay]==NO)
	{
		machine.drawingFadeDelay=delay;
		machine.modified=[NSDate date];
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:machine];
	}
}
	   
+ (void)setMachine:(FGMachine*)machine drawingFadeDuration:(NSNumber*)duration
{
	if([NSNumber isEqualNumber:machine.drawingFadeDuration toNumber:duration]==NO)
	{
		machine.drawingFadeDuration=duration;
		machine.modified=[NSDate date];
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:machine];
	}
   
}

+ (void)setMachine:(FGMachine*)machine thumbData:(NSData*)data
{
	if(machine.thumb!=data)
	{
		machine.thumb=data;
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:machine];
	}
}

+ (void)setMachineState:(FGMachineState*)machineState running:(BOOL)running
{
	if(machineState.running!=running)
	{
		machineState.running=running;
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationRunningStateChanged object:machineState];
	}
}

+ (void)setMachineState:(FGMachineState*)machineState timeOffset:(NSTimeInterval)timeOffset
{
	if(machineState.timeOffset!=timeOffset)
	{
		FGGearState *gearState=machineState.topLevelGearState;
		if(timeOffset==0)
		{
			// the reason we handle 0 offset differently is to correct possible aberrations due to rpm rate changes during playback.
			for(; gearState!=nil; gearState=gearState.child)
			{
				gearState.rotationOffset=0;
			}
		}
		else
		{
			NSTimeInterval minuteDelta=(timeOffset-machineState.timeOffset)/60;
			for(; gearState!=nil; gearState=gearState.child)
			{
				gearState.rotationOffset+=[gearState.gear.rpm floatValue] * minuteDelta;
			}
		}

		machineState.timeOffset=timeOffset;
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationMachineStateChanged object:machineState];
	}
}

/**** gear interface ****/
+ (void)removeGearState:(FGGearState*)gearState machine:(FGMachine*)machine
{
	[gearState remove];
	machine.modified=[NSDate date];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationMachineStateChanged object:gearState];
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:gearState.gear];
}

+ (void)setGearSelected:(FGGearState*)gearState machine:(FGMachineState*)machineState
{
	if([machineState getSelectedGearState]!=gearState)
	{
		[machineState setSelectedGearState:gearState];
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationSelectionStateChanged object:gearState];
	}
}

+ (void)setGear:(FGGear*)gear rpm:(float)rpm machine:(FGMachine*)machine
{
	NSNumber *value=[NSNumber numberWithFloat:rpm];
	if([gear.rpm isEqualToNumber:value]==NO)
	{
		// note: changes during playback will cause the rotationOffset to be inaccurate.  We intentionally bear it (looks ridiculous during playback otherwise).
		gear.rpm=value;
		machine.modified=[NSDate date];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:gear];
	}
}

+ (void)setGear:(FGGear*)gear radiusRatio:(float)radiusRatio machine:(FGMachine*)machine
{
	NSNumber *value=[NSNumber numberWithFloat:radiusRatio];
	if([gear.radiusRatio isEqualToNumber:value]==NO)
	{
		gear.radiusRatio=value;
		machine.modified=[NSDate date];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:gear];
	}
}

+ (void)setGear:(FGGear*)gear penCount:(int)penCount machine:(FGMachine*)machine
{
	NSNumber *value=[NSNumber numberWithInt:penCount];
	if([gear.penCount isEqualToNumber:value]==NO)
	{
		gear.penCount=value;
		machine.modified=[NSDate date];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:gear];
	}
}


@end


