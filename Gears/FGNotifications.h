//
//  Notifications.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

/**** error notifications ****/
extern NSString *const NotificationDebug;
extern NSString *const NotificationInfo;
extern NSString *const NotificationWarn;
extern NSString *const NotificationError;

/**** model IO notifications ****/
extern NSString *const NotificationDBOpening;
extern NSString *const NotificationDBOpenSucceeded;
extern NSString *const NotificationDBOpenFailed;
extern NSString *const NotificationDBSaving;
extern NSString *const NotificationDBSaveSucceeded;
extern NSString *const NotificationDBSaveFailed;

/**** model and state modification notifications ****/
extern NSString *const NotificationDBModified;

extern NSString *const NotificationRunningStateChanged;
extern NSString *const NotificationMachineStateChanged;
extern NSString *const NotificationSelectionStateChanged;

