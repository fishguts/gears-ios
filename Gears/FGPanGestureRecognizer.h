//
//  FGPanGestureRecognizer.h
//  Junk
//
//  Created by Curtis Elsasser on 3/25/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
	FGPanDirectionUnknown,
	FGPanDirectionHorizontal,
	FGPanDirectionVertical
} FGPanDirection;


@interface FGPanGestureRecognizer : UIPanGestureRecognizer
{
	CGPoint _locationBegan;
}

@property (nonatomic, readonly) FGPanDirection direction;

@end
