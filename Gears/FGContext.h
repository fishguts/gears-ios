//
//  FGContext.h
//  Gears
//
//  Created by Curtis Elsasser on 3/27/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@interface FGContext : FGStaticClass
+ (void)showTextAtPoint:(CGPoint)point text:(NSString*)text context:(CGContextRef)context;
+ (void)showTextAtPoint:(CGPoint)point text:(NSString *)text fontFamily:(NSString*)fontFamily fontSize:(CGFloat)fontSize context:(CGContextRef)context;
+ (void)showTextAtPoint:(CGPoint)point text:(NSString *)text fontFamily:(NSString*)fontFamily fontSize:(CGFloat)fontSize rotation:(CGFloat)rotation context:(CGContextRef)context;

@end
