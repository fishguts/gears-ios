//
//  FGFrameCalculator.h
//  Gears
//
//  Created by Curtis Elsasser on 5/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGFrameCalculator : NSObject
/**** properties ****/
+ (double)frameRateWithInterval:(int)interval;

/**** interface ****/
+ (double)frameCountWithInterval:(int)interval duration:(NSTimeInterval)duration;

@end
