//
//  FGViewController.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import "FGMachineController.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGMachineView.h"
#import "FGMachineProxy.h"
#import "FGMachineFactory.h"
#import "FGMachineGeometry.h"
#import "FGMachineSerializer.h"
#import "FGGear.h"
#import "FGGear+Math.h"
#import "FGGearState.h"
#import "FGDefaults.h"
#import "FGNotifications.h"
#import "FGPanGestureRecognizer.h"
#import "NSNotification+FG.h"

#import <QuartzCore/QuartzCore.h>


@interface FGMachineController()
{
@private 
	float _selectedGearPenCount;
	BOOL _gearsHiddenState;
	BOOL _savedRunningState;
}

/**** properties ****/
@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, strong) FGMachineState *machineState;
@property (weak, nonatomic) IBOutlet FGMachineView *machineView;
@property (weak, nonatomic) IBOutlet UIButton *buttonRemoveGear;
@property (weak, nonatomic) IBOutlet UIButton *buttonAddGear;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonStart;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonReset;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonHide;

/**** private workers ****/
- (void)startup;
- (void)shutdown;
- (void)addObservers;
- (void)removeObservers;

- (FGGearState*)getSelectedGearOnlyIfEditable;

- (void)selectionStateToUI;
- (void)updateMachineThumb;

/**** observers ****/
- (void)handleDBModifiedNotification:(NSNotification*)notification;
- (void)handleRunningStateChangedNotification:(NSNotification*)notification;
- (void)handleMachineStateChangedNotification:(NSNotification*)notification;
- (void)handleSelectionStateChangedNotification:(NSNotification*)notification;

- (IBAction)handlePanGesture:(UIPanGestureRecognizer*)sender;

- (void)handleAdvanceMachine:(CADisplayLink*)link;

@end


@implementation FGMachineController

/**** properties ****/
@synthesize machine=_machine;
@synthesize machineState;
@synthesize displayLink;
@synthesize buttonRemoveGear;
@synthesize buttonAddGear;
@synthesize machineView;
@synthesize buttonStart;
@synthesize buttonReset;
@synthesize buttonHide;


- (void)setMachine:(FGMachine*)machine
{
	if(machine!=self->_machine)
	{
		self->_machine=machine;
		self.machineState=[FGMachineFactory createMachineState:machine];
		self.machineView.machineState=self.machineState;
#if DEBUG
		 NSLog(@"Machine->JSON '%@':\n%@\n", machine.title, [FGMachineSerializer machineToJSON:machine pretty:NO]);
#endif
	}
}

#pragma mark - Private API
- (void)startup
{
	self.displayLink=[CADisplayLink displayLinkWithTarget:self selector:@selector(handleAdvanceMachine:)];
	self.displayLink.frameInterval=FGDefaultAnimationFrameInterval;
	[self addObservers];
}

- (void)shutdown
{
	[self removeObservers];
}

- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];

	// db/state change notification
	[center addObserver:self selector:@selector(handleDBModifiedNotification:) name:NotificationDBModified object:nil];
	[center addObserver:self selector:@selector(handleRunningStateChangedNotification:) name:NotificationRunningStateChanged object:nil];
	[center addObserver:self selector:@selector(handleMachineStateChangedNotification:) name:NotificationMachineStateChanged object:nil];
	[center addObserver:self selector:@selector(handleSelectionStateChangedNotification:) name:NotificationSelectionStateChanged object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (FGGearState*)getSelectedGearOnlyIfEditable
{
	if(self->_gearsHiddenState)
	{
		return nil;
	}
	FGGearState *gear=[self.machineState getSelectedGearState];
	return (gear!=self.machineState.topLevelGearState) ? gear : nil;
}

- (void)updateMachineThumb
{
	UIImage *image=[self.machineView imageFromGears:CGSizeMake(32, 32)];
	NSData *data=UIImagePNGRepresentation(image);
	[FGMachineProxy setMachine:self.machine thumbData:data];
}

- (void)selectionStateToUI
{
	if(self->_gearsHiddenState)
	{
		[self.buttonAddGear setEnabled:NO];
		[self.buttonRemoveGear setEnabled:NO];
		[self.machineView hideInfoLayer:YES];
	}
	else
	{
		FGGearState *selectedGear=[self.machineState getSelectedGearState];
		if(selectedGear==nil)
		{
			[self.buttonAddGear setEnabled:NO];
			[self.buttonRemoveGear setEnabled:NO];
			[self.machineView hideInfoLayer:YES];
		}
		else
		{
			[self.buttonAddGear setEnabled:(selectedGear.child==nil) && [FGMachineGeometry canGearContainChild:selectedGear.gear totalBounds:self.machineView.topLevelGearBounds]];
			if(selectedGear!=self.machineState.topLevelGearState)
			{
				[self.buttonRemoveGear setEnabled:YES];
				[self.machineView showInfoLayerWithGear:selectedGear infoType:FGInfoOverlayRPMandPenCount pulse:YES];
			}
			else
			{
				[self.buttonRemoveGear setEnabled:NO];
				[self.machineView hideInfoLayer:YES];
			}
		}
	}
}

#pragma mark - View lifecycle and overrides
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.destinationViewController respondsToSelector:@selector(setMachine:)])
	{
		[segue.destinationViewController performSelector:@selector(setMachine:) withObject:self.machine];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self startup];
	
	// note: our view is not necessarily created at the time our machine is set. 
	self.machineView.machineState=self.machineState;
	if([self.machineState getSelectedGearState]==nil)
	{
		FGGearState *selectedGear=(self.machineState.topLevelGearState.child!=nil) 
			? self.machineState.topLevelGearState.child : self.machineState.topLevelGearState;
		[FGMachineProxy setGearSelected:selectedGear machine:self.machineState];
	}
}

- (void)viewWillUnload
{
	[super viewWillUnload];
}

- (void)viewDidUnload
{
	[self shutdown];
	[self setMachineView:nil];
	[self setButtonStart:nil];
	[self setButtonReset:nil];
	[self setButtonAddGear:nil];
	[self setButtonRemoveGear:nil];
	[self setButtonHide:nil];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[FGMachineProxy setMachineState:self.machineState running:self->_savedRunningState];
}

- (void)viewWillDisappear:(BOOL)animated
{
	// not very future friendly: https://discussions.apple.com/thread/1660161?threadID=1660161
	const BOOL exiting=([self.navigationController.viewControllers indexOfObject:self]==NSNotFound);
	
	// wherever we are going stop playback
	self->_savedRunningState=self.machineState.running;
	[FGMachineProxy setMachineState:self.machineState running:NO];

	if(exiting)
	{
		[self updateMachineThumb];
		[self shutdown];
	}
	[super viewWillDisappear:animated];
}

#pragma mark - Observers/Handlers
- (void)handleDBModifiedNotification:(NSNotification*)notification
{
	[self.machineView validateDrawingMethod];
	[self.machineView redrawMachineState];
}

- (void)handleRunningStateChangedNotification:(NSNotification*)notification
{
	if(self.machineState.running)
	{
		self.buttonStart.title=@"Stop";
		[self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	}
	else
	{
		self.buttonStart.title=@"Start";
		[self.displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	}
	[self.machineView redrawMachineState];
}

- (void)handleMachineStateChangedNotification:(NSNotification*)notification
{
	[self.machineView redrawMachineState];
}

- (void)handleSelectionStateChangedNotification:(NSNotification*)notification
{
	[self selectionStateToUI];
	// force the new selection to be drawn
	[self.machineView redrawMachineState];
}

- (void)handleAdvanceMachine:(CADisplayLink*)link
{
	const NSTimeInterval timeOffset=self.machineState.timeOffset+(link.duration*link.frameInterval);
	
	[self.machineView preMachineAdvance];
	[FGMachineProxy setMachineState:machineState timeOffset:timeOffset];
	[self.machineView postMachineAdvance];
}

/**** outlet actions ****/
- (IBAction)handleTapGesture:(UITapGestureRecognizer*)sender 
{
	if(self->_gearsHiddenState==NO)
	{
		FGGearState *gearState=[self.machineView gearStateFromPoint:[sender locationInView:self.machineView] withRadius:FGDefaultFingerRadius];
		[FGMachineProxy setGearSelected:gearState machine:self.machineState];
	}
}

- (IBAction)handlePinchGesture:(UIPinchGestureRecognizer*)sender 
{
	if((sender.state==UIGestureRecognizerStateChanged)
	   ||(sender.state==UIGestureRecognizerStateEnded))
	{
		FGGearState *selectedGear=[self getSelectedGearOnlyIfEditable];
		if(selectedGear!=nil)
		{
			// update selected gear size
			CGSize bounds=self.machineView.topLevelGearBounds.size;
			float radiusRatio=sender.scale*[selectedGear.gear.radiusRatio floatValue];
			radiusRatio=fmaxf([selectedGear.gear radiusRatioMinWithTotalBounds:bounds], fminf(radiusRatio, [selectedGear.gear radiusRatioMaxWithTotalBounds:bounds]));
			[FGMachineProxy setGear:selectedGear.gear radiusRatio:radiusRatio machine:self.machine];
			
			// use the following to test rotation math. Looks good right now (4/2012)
			// NSLog(@"gear outter circumference=%f, parent inner circumference=%f", [selectedGear.gear outterCircumferenceWithTotalBounds:bounds], [selectedGear.gear.parent innerCircumferenceWithTotalBounds:bounds ringWidth:FGDefaultGearRingWidth]);
			if(sender.state==UIGestureRecognizerStateEnded)
			{
				// update add gear state
				if(selectedGear.child==nil)
				{
					[self.buttonAddGear setEnabled:[FGMachineGeometry canGearContainChild:selectedGear.gear totalBounds:self.machineView.topLevelGearBounds]];
				}
			}
		}
		// reset so that next value is relative to current pinch
		sender.scale=1;
	}
}

- (IBAction)handlePanGesture:(FGPanGestureRecognizer*)sender 
{
	FGGearState *selectedGear=[self getSelectedGearOnlyIfEditable];
	if(selectedGear!=nil)
	{
		if(sender.state==UIGestureRecognizerStateBegan)
		{
			self->_selectedGearPenCount=[selectedGear.gear.penCount floatValue];
			// show the info overlay for whatever it is we are altering
			if(sender.direction==FGPanDirectionHorizontal)
			{
				[self.machineView showInfoLayerWithGear:selectedGear infoType:FGInfoOverlayPenCount pulse:NO];
			}
			else
			{
				[self.machineView showInfoLayerWithGear:selectedGear infoType:FGInfoOverlayRPM pulse:NO];
			}
		}
		else if((sender.state==UIGestureRecognizerStateChanged)
		   ||(sender.state==UIGestureRecognizerStateEnded))
		{
			CGPoint translatedPoint=[sender translationInView:self.view];
			if(sender.direction==FGPanDirectionHorizontal)
			{
				self->_selectedGearPenCount=fmaxf(FGMinGearPenCount, fminf(FGMaxGearPenCount, self->_selectedGearPenCount+translatedPoint.x/20));
				[FGMachineProxy setGear:selectedGear.gear penCount:self->_selectedGearPenCount machine:self.machine];
				// update information layer state and reset translation
				[self.machineView updateInfoLayer];
				[sender setTranslation:CGPointZero inView:self.view];
			}
			else
			{
				float rpmOld=[selectedGear.gear.rpm floatValue];
				// 5 pixels of vertical motion per each RPM
				float rpmNew=fmaxf(FGMinGearRPM, fminf(FGMaxGearRPM, roundf(rpmOld-translatedPoint.y/5.0)));
				
				// if the value did not change then don't bother with the rest, specifically resetting the translation point, otherwise we can 
				// sort of get stuck on a number when panning slowly due to retinas points vs. pixels business.
				if(rpmOld!=rpmNew)
				{
					[FGMachineProxy setGear:selectedGear.gear rpm:rpmNew machine:self.machine];
					// update information layer state and reset translation
					[self.machineView updateInfoLayer];
					[sender setTranslation:CGPointZero inView:self.view];
				}
			}
			
			if(sender.state==UIGestureRecognizerStateEnded)
			{
				[self.machineView hideInfoLayer:YES];
			}
		}
	}
}

- (IBAction)handleAddGear:(UIButton*)sender 
{
	FGGearState *gearState=[FGMachineFactory createGearStateWithParent:[self.machineState getSelectedGearState] machine:self.machineState];
	[FGMachineProxy setGearSelected:gearState machine:machineState];
}

- (IBAction)handleRemoveGear:(UIButton*)sender 
{
	FGGearState *gearStateRemove=[self.machineState getSelectedGearState];
	FGGearState *gearStateSelect=gearStateRemove.parent;

	// note: saving selection for last so that states are finalized before new selection is sent out.
	[FGMachineProxy removeGearState:gearStateRemove machine:self.machine];
	[FGMachineProxy setGearSelected:gearStateSelect machine:self.machineState];
}


- (IBAction)handleStart:(UIBarItem*)sender 
{
	[FGMachineProxy setMachineState:machineState running:!self.machineState.running];
}

- (IBAction)handleReset:(UIBarItem*)sender 
{
	[self.machineView clearDrawing];
	[FGMachineProxy setMachineState:machineState running:NO];
	[FGMachineProxy setMachineState:machineState timeOffset:0];
}

- (IBAction)handleHide:(UIBarItem*)sender 
{
	self->_gearsHiddenState=!self->_gearsHiddenState;
	if(self->_gearsHiddenState)
	{
		self.buttonHide.title=@"Reveal";
		self.buttonHide.tintColor=[UIColor redColor];
		[self.machineView hideGearLayer:YES];
	}
	else
	{
		self.buttonHide.title=@"Hide";
		self.buttonHide.tintColor=nil;
		[self.machineView showGearLayer:YES];
	}
	[self selectionStateToUI];
}
	 

@end
