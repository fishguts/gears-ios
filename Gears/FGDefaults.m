//
//  FGConstants.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDefaults.h"

static UIColor *gearColorNormal;
static UIColor *gearColorSelected;
static UIColor *overlayColorFill;
static UIColor *overlayColorText;

// note: turning splash off for now as there is no essential info there at the moment.
#if DEBUG
	const float FGDefaultAnimationFrameInterval=2;
	const NSTimeInterval FGDefaultSplashDuration=0.5;
#else
	const float FGDefaultAnimationFrameInterval=2;
	const NSTimeInterval FGDefaultSplashDuration=1.5;
#endif

// note: it looks like UIPanGestureRecognizer has a built in threshold of 10.
const float FGDefaultPanThreshold=5;
const float FGDefaultGearRingWidth=10;
const float FGDefaultPenRadius=3;
const float FGDefaultFingerRadius=5;
const CGSize FGDefaultImageThumbSize={ 32, 32 };
NSString *const FGDefaultOverlayFontFamily=@"Futura-CondensedExtraBold";


const float FGDefaultGearRPM=10.0; 
const float FGMinGearRPM=-500.0; 
const float FGMaxGearRPM=500.0; 

const int FGDefaultGearPenCount=2;
const int FGMinGearPenCount=0;
const int FGMaxGearPenCount=25;


@implementation FGDefaults

+ (UIColor*)getGearColorNormal
{
	if(gearColorNormal==nil)
	{
		gearColorNormal=[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
	}
	return gearColorNormal;
}

+ (UIColor*)getGearColorSelected
{
	if(gearColorSelected==nil)
	{
		gearColorSelected=[UIColor colorWithRed:0.0 green:1.0 blue:1.0 alpha:1.0];
	}
	return gearColorSelected;
}

+ (UIColor*)getOverlayColorFill
{
	if(overlayColorFill==nil)
	{
		overlayColorFill=[UIColor colorWithRed:0.175 green:0.175 blue:0.175 alpha:1.0];
	}
	return overlayColorFill;
}

+ (UIColor*)getOverlayColorText
{
	if(overlayColorText==nil)
	{
		overlayColorText=[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
	}
	return overlayColorText;
}

@end