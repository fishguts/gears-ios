//
//  FGSplashController.h
//  Gears
//
//  Created by Curtis Elsasser on 5/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@interface FGSplashController : UIViewController

@property (nonatomic, weak) id <FGModalDelegate> delegate;

@end
