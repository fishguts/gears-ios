//
//  FGDefaults.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

/**** preprocessor defines (see project) ****/
// FREE_BUILD
// PAYED_BUILD


/**** application constants ****/
extern const NSTimeInterval FGDefaultSplashDuration;
extern const float FGDefaultAnimationFrameInterval;
extern const float FGDefaultPanThreshold;
extern const float FGDefaultGearRingWidth;
extern const float FGDefaultPenRadius;
extern const float FGDefaultFingerRadius;
extern const CGSize FGDefaultImageThumbSize;
extern NSString *const FGDefaultOverlayFontFamily;

extern const float FGDefaultGearRPM; 
extern const float FGMinGearRPM; 
extern const float FGMaxGearRPM; 

extern const int FGDefaultGearPenCount;
extern const int FGMinGearPenCount;
extern const int FGMaxGearPenCount;

// todo: is there a way to make runtime constants?
@interface FGDefaults
+ (UIColor*)getGearColorNormal;
+ (UIColor*)getGearColorSelected;
+ (UIColor*)getOverlayColorFill;
+ (UIColor*)getOverlayColorText;
@end
