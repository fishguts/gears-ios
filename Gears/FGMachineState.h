//
//  FGMachineState.h
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGMachine;
@class FGGearState;

@interface FGMachineState : NSObject

/**** properties ****/
@property (nonatomic, readonly, weak) FGMachine *machine;
@property (nonatomic, readonly, strong) FGGearState *topLevelGearState;
@property (nonatomic) NSTimeInterval timeOffset;
@property (nonatomic) BOOL running;

/**** instance methods ****/
- (id)initWithMachine:(FGMachine*)machine;
- (FGGearState*)getSelectedGearState;
- (void)setSelectedGearState:(FGGearState*)gearState;


@end
