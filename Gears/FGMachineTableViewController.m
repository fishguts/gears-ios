//
//  FGMachineTableViewController.m
//  Gears
//
//  Created by Curtis Elsasser on 4/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineTableViewController.h"
#import "FGTextEditorTableViewCell.h"
#import "FGEditorPickerController.h"
#import "FGEditorDataFactory.h"
#import "FGMachineProxy.h"
#import "FGMachine.h"
#import "NSString+FGFormat.h"


@implementation FGMachineTableViewController

#pragma mark - Properties
@synthesize machine=_machine;
@synthesize delegate=_delegate;

- (void)setMachine:(FGMachine *)machine
{
	self->_machine=machine;
	self->_fadeDelay=(machine.drawingFadeDelay!=nil) ? [machine.drawingFadeDelay floatValue] : FLT_MIN;
	self->_fadeDuration=(machine.drawingFadeDuration!=nil) ? [machine.drawingFadeDuration floatValue] : FLT_MIN;
}

#pragma mark - overrides
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	FGEditorPickerController *pickerController;
	FGTextEditorTableViewCell *cell=(FGTextEditorTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	
	[cell.propertyValue resignFirstResponder];
	if([segue.identifier isEqualToString:@"FadeDelaySegue"])
	{
		[FGEditorDataFactory getFadeDelayPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Fade Delay";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=154.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatFadeDelay:self->_fadeDelay];
	}
	else if([segue.identifier isEqualToString:@"FadeDurationSegue"])
	{
		[FGEditorDataFactory getFadeDurationPrimitiveValues:&self->_editingDataValues friendlyValues:&self->_editingFriendlyValues];
		pickerController=segue.destinationViewController;
		pickerController.title=@"Fade Duration";
		pickerController.delegate=self;
		pickerController.identifier=segue.identifier;
		pickerController.dataSourceColumnWidth=154.0;
		pickerController.dataSourceColumnAlignment=UITextAlignmentCenter;
		pickerController.dataSource=self->_editingFriendlyValues;
		pickerController.selectedDataItem=[NSString formatFadeDuration:self->_fadeDuration];
	}
}

#pragma mark - view lifecycle 
- (void)viewWillDisappear:(BOOL)animated
{
	FGTextEditorTableViewCell *cell=(FGTextEditorTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
	
	[FGMachineProxy setMachine:self.machine title:cell.propertyValue.text];
	[FGMachineProxy setMachine:self.machine drawingFadeDelay:(self->_fadeDelay!=FLT_MIN) ? [NSNumber numberWithFloat:self->_fadeDelay] : nil];
	[FGMachineProxy setMachine:self.machine drawingFadeDuration:(self->_fadeDuration!=FLT_MIN) ? [NSNumber numberWithFloat:self->_fadeDuration] : nil];

	[self.delegate editorComplete:self];
	[super viewWillDisappear:animated];
}

- (void)editorComplete:(id)editor
{
	NSString *identifier=[editor identifier];
	if([identifier isEqualToString:@"FadeDelaySegue"])
	{
		self->_fadeDelay=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	else if([identifier isEqualToString:@"FadeDurationSegue"])
	{
		self->_fadeDuration=[[self->_editingDataValues objectAtIndex:[editor selectedRow]] floatValue];
	}
	[self.tableView reloadData];
	
	// clean up
	self->_editingFriendlyValues=nil;
	self->_editingDataValues=nil;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 5; 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell;
	if(indexPath.section==0)
	{
		if(indexPath.row==0)
		{
			cell=[tableView dequeueReusableCellWithIdentifier:@"ReadWrite"];
			((FGTextEditorTableViewCell*)cell).propertyName.text=@"Title";
			((FGTextEditorTableViewCell*)cell).propertyValue.text=self.machine.title;
		}
		else if(indexPath.row==1)
		{
			cell=[tableView dequeueReusableCellWithIdentifier:@"FadeDelay"];
			cell.detailTextLabel.text=[NSString formatFadeDelay:self->_fadeDelay];
		}
		else if(indexPath.row==2)
		{
			cell=[tableView dequeueReusableCellWithIdentifier:@"FadeDuration"];
			cell.detailTextLabel.text=[NSString formatFadeDuration:self->_fadeDuration];
		}
		else if(indexPath.row==3)
		{
			cell=[tableView dequeueReusableCellWithIdentifier:@"Readonly"];
			cell.textLabel.text=@"Created";
			cell.detailTextLabel.text=[NSDateFormatter localizedStringFromDate:self.machine.created dateStyle:kCFDateFormatterMediumStyle timeStyle:kCFDateFormatterMediumStyle];
		}
		else if(indexPath.row==4)
		{
			cell=[tableView dequeueReusableCellWithIdentifier:@"Readonly"];
			cell.textLabel.text=@"Modified";
			cell.detailTextLabel.text=[NSDateFormatter localizedStringFromDate:self.machine.modified dateStyle:kCFDateFormatterMediumStyle timeStyle:kCFDateFormatterMediumStyle];
		}
	}
	else
	{
		NSAssert(NO, @"Fail");
	}
    
    return cell;
}

@end


