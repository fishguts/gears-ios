//
//  FGDrawingLayer.h
//  Gears
//
//  Created by Curtis Elsasser on 3/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBaseLayer.h"

@class FGMachineState;

@interface FGDrawingLayer : FGBaseLayer

/**** properties ****/
@property (nonatomic, weak) FGMachineState *machineState;

/**** public interface ****/
- (void)clearLayer;
- (void)preMachineAdvance;
- (void)postMachineAdvance;

@end
