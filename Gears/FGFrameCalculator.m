//
//  FGFrameCalculator.m
//  Gears
//
//  Created by Curtis Elsasser on 5/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGFrameCalculator.h"

// as best I can tell the frame rate is constant and not query'able
static const double REFRESH_RATE=60.0;

@implementation FGFrameCalculator

+ (double)frameRateWithInterval:(int)interval
{
	return REFRESH_RATE/interval;
}

+ (double)frameCountWithInterval:(int)interval duration:(NSTimeInterval)duration
{
	return (duration*REFRESH_RATE)/interval;
}

@end
