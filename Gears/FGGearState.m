//
//  FGGearState.m
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGearState.h"
#import "FGGear.h"

static NSUInteger nextGearStateId=1;

@implementation FGGearState

/**** properties ****/
@synthesize gear=_gear;
@synthesize parent=_parent;
@synthesize child=_child;
@synthesize id=_id;
@synthesize rotationOffset=_rotationOffset;
@synthesize selected=_selected;

- (id)initWithGear:(FGGear*)gear parent:(FGGearState*)parent
{
	self=[super init];
	self->_gear=gear;
	self->_id=[NSString stringWithFormat:@"%d", nextGearStateId++];
	self.parent=parent;
	parent.child=self;
	
	return self;
}

- (void)remove
{
	// db set to cascade
	[self.child remove];
	
	self.gear.parent.child=nil;
	self.gear.parent=nil;
	
	self.parent.child=nil;
	self.parent=nil;
}

@end
