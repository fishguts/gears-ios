//
//  NSNotification+FGFactory.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSNotification+FG.h"
#import "FGNotifications.h"

@implementation NSNotification (FGAlert)
+ (id)debugNotificationWithObject:(id)object text:(NSString*)text
{
	return [NSNotification notificationWithName:NotificationDebug object:object userInfo:[NSDictionary dictionaryWithObject:text forKey:@"text"]];
}

+ (id)infoNotificationWithObject:(id)object text:(NSString*)text
{
	return [NSNotification notificationWithName:NotificationInfo object:object userInfo:[NSDictionary dictionaryWithObject:text forKey:@"text"]];
}

+ (id)warnNotificationWithObject:(id)object text:(NSString*)text
{
	return [NSNotification notificationWithName:NotificationWarn object:object userInfo:[NSDictionary dictionaryWithObject:text forKey:@"text"]];
}

+ (id)errorNotificationWithObject:(id)object text:(NSString*)text
{
	return [NSNotification notificationWithName:NotificationError object:object userInfo:[NSDictionary dictionaryWithObject:text forKey:@"text"]];
}

- (NSString*)alertText
{
	return [[self userInfo] objectForKey:@"text"];
}

@end

@implementation NSNotification (FGDatabase)
+ (id)databaseModifiedWithObject:(id)object autoSave:(BOOL)autoSave
{
	return [NSNotification notificationWithName:NotificationDBModified object:object userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:autoSave] forKey:@"autoSave"]];
}

- (BOOL)isDatabaseAutoSave
{
	NSNumber *value=[[self userInfo] objectForKey:@"autoSave"];
	return [value boolValue];
}

@end
