//
//  NSString+FGFormat.h
//  Gears
//
//  Created by Curtis Elsasser on 5/8/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FGFormat)
+ (NSString*)formatGearCount:(int)value;
+ (NSString*)formatSizeRatio:(float)value;
+ (NSString*)formatRPM:(float)value;
+ (NSString*)formatRPMRatio:(float)value;
+ (NSString*)formatFadeDelay:(float)value;
+ (NSString*)formatFadeDuration:(float)value;

@end
