//
//  FGDrawingLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDrawingFadeLayer.h"
#import "FGDrawingPath.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGGear.h"
#import "FGGearState.h"
#import "FGMachineGeometry.h"
#import "FGFrameCalculator.h"
#import "FGDefaults.h"

/* Cache 256 alphas.  This was the heaviest hit when benchmarking */
static NSMutableArray *staticAlphaArray;

@interface FGDrawingFadeLayer()
{
@private
	int _frameOffset;
	NSMutableDictionary *_gearToPathsMap;
	NSMutableArray *_removedPaths;
}

/**** utilities ****/
- (int)getPathPointCountMax;
- (int)getPathPointCountFadeDelay;
- (int)getPathPointCountFadeDuration;

/**** private interface ****/
- (void)trimPathPoints:(FGDrawingPath*)path;
- (void)drawPaths:(NSArray*)paths context:(CGContextRef)context;
@end


@implementation FGDrawingFadeLayer

+ (void)initialize
{
	staticAlphaArray=[NSMutableArray arrayWithCapacity:256];
	for(float index=0; index<256; index++)
	{
		[staticAlphaArray addObject:[UIColor colorWithRed:0 green:0 blue:0 alpha:index/256.0]];
	}
}

/**** public interface ****/
- (id)init
{
	self=[super init];
	if(self!=nil)
	{
		self->_gearToPathsMap=[NSMutableDictionary dictionary];
		self->_removedPaths=[NSMutableArray array];
	}
	return self;
}

- (void)dealloc
{
	[self clearLayer];
}

- (void)clearLayer
{
	[self->_gearToPathsMap removeAllObjects];
	[self->_removedPaths removeAllObjects];
	self->_frameOffset=0;
	[self setNeedsDisplay];
}

- (void)preMachineAdvance
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;
	
	while(gearState!=nil) 
	{
		FGDrawingPath *path;
		const int penCount=[gearState.gear.penCount intValue];
		NSMutableArray *paths=[self->_gearToPathsMap objectForKey:gearState.id];
		
		// make sure we've got the path array registered (just starting or new gear).
		if(paths==nil)
		{
			paths=[NSMutableArray array];
			[self->_gearToPathsMap setObject:paths forKey:gearState.id];
		}
		
		// note: must get each gear's bounds to find the next gears bounds.
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		
		// the following block creates inital points and removes no longer referenced pens
		// If we've already got the paths started then we are done for this gear.
		if([paths count]!=penCount)
		{
			if([paths count]<penCount)
			{
				CGPoint points[penCount];
				[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
				
				// and paths and points
				for(int penIndex=[paths count]; penIndex<penCount; penIndex++)
				{
					path=[[FGDrawingPath alloc] initWithFrameStart:self->_frameOffset];
					[path addPoint:points[penIndex] frame:self->_frameOffset];
					[paths addObject:path];
				}
			}
			else
			{
				// points that have been removed
				while([paths count]>penCount)
				{
					[self->_removedPaths addObject:[paths lastObject]];
					[paths removeLastObject];
				}
			}
		}
		
		// advance
		gearState=gearState.child;
	}
}

- (void)postMachineAdvance
{
	FGDrawingPath *path;
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	// advance the machine
	self->_frameOffset+=FGDefaultAnimationFrameInterval;
	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;
	
	while(gearState!=nil)
	{
		int penCount=[gearState.gear.penCount intValue];
		
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		if(penCount>0)
		{
			CGPoint points[penCount];
			NSMutableArray *paths=[self->_gearToPathsMap objectForKey:gearState.id];
			
			[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
			for(int penIndex=0; penIndex<penCount; penIndex++)
			{
				path=[paths objectAtIndex:penIndex];
				[path addPoint:points[penIndex] frame:self->_frameOffset];
				[self trimPathPoints:path];
			}
			
		}
		// advance
		gearState=gearState.child;
	}
	
	// process removed paths
	for(int pathIndex=[self->_removedPaths count]-1; pathIndex>-1; pathIndex--)
	{
		path=[self->_removedPaths objectAtIndex:pathIndex];
		[self trimPathPoints:path];
		if([path getPointCount]==0)
		{
			[self->_removedPaths removeObjectAtIndex:pathIndex];
		}
	}
	
}

/**** utilities ****/
- (int)getPathPointCountMax
{
	FGMachine *machine=self.machineState.machine;
	const float frameRate=[FGFrameCalculator frameRateWithInterval:FGDefaultAnimationFrameInterval];
	
	if(machine.drawingFadeDelay!=nil)
	{
		return [machine.drawingFadeDelay floatValue]*frameRate + [machine.drawingFadeDuration floatValue]*frameRate;
	}
	return INT32_MAX;
}

- (int)getPathPointCountFadeDelay
{
	FGMachine *machine=self.machineState.machine;
	const float frameRate=[FGFrameCalculator frameRateWithInterval:FGDefaultAnimationFrameInterval];

	if(machine.drawingFadeDelay!=nil)
	{
		return [machine.drawingFadeDelay floatValue]*frameRate;
	}
	return INT32_MAX;
}

- (int)getPathPointCountFadeDuration
{
	FGMachine *machine=self.machineState.machine;
	const float frameRate=[FGFrameCalculator frameRateWithInterval:FGDefaultAnimationFrameInterval];

	// note: intentionally look at fade delay.  If there is one then a nil equates to immediate fade.
	if(machine.drawingFadeDelay!=nil)
	{
		return [machine.drawingFadeDuration floatValue]*frameRate;
	}
	return INT32_MAX;
}

/**** path drawing and management ****/
- (void)trimPathPoints:(FGDrawingPath*)path
{
	const int count=[self getPathPointCountMax]-(self->_frameOffset-path.frameLastPoint);
	const int remove=[path getPointCount]-count;
	if(remove>0)
	{
		[path removePointsFromBeginning:remove];
	}
}

- (void)drawPaths:(NSArray *)paths context:(CGContextRef)context
{
	int index;
	CGPoint point;
	
	const int fadeDelayPointCount=[self getPathPointCountFadeDelay];
	
	for(FGDrawingPath *path in paths)
	{
		const int pointCount=[path getPointCount];
		const int pathDelayPointCount=MAX(0, fadeDelayPointCount-(self->_frameOffset-path.frameLastPoint));
		const int pathFadePointCount=MAX(0, pointCount-pathDelayPointCount);

		// draw faded portion
		if(pathFadePointCount>0)
		{
			for(index=0; index<(pathFadePointCount-1); index++)
			{
				const int alphaIndex=(index*256)/(pathFadePointCount-1);
				UIColor *color=[staticAlphaArray objectAtIndex:alphaIndex];
				
				CGContextBeginPath(context);
				CGContextSetStrokeColorWithColor(context, [color CGColor]);
				{
					point=[path getPointAt:index];
					CGContextMoveToPoint(context, point.x, point.y);

					point=[path getPointAt:index+1];
					CGContextAddLineToPoint(context, point.x, point.y);
				}
				CGContextStrokePath(context);
			}
		}
		
		// draw solid portion
		if(pathDelayPointCount>0)
		{
			// join to our last point if we are extending a faded path
			index=MAX(0, pathFadePointCount-1);

			CGContextBeginPath(context);
			CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
			CGContextAddLines(context, [path getPointBufferAt:index], pointCount-index);
			CGContextStrokePath(context);
		}
	}
}

- (void)drawInContext:(CGContextRef)context
{
	for(NSArray *penPaths in [self->_gearToPathsMap allValues])
	{
		[self drawPaths:penPaths context:context];
	}
	[self drawPaths:self->_removedPaths context:context];
}

@end
