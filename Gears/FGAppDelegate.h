//
//  FGAppDelegate.h
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
