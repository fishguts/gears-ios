//
//  main.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FGAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([FGAppDelegate class]));
	}
}
