//
//  FGSplashController.m
//  Gears
//
//  Created by Curtis Elsasser on 5/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSplashController.h"
#import "FGDefaults.h"

@interface FGSplashController()

/**** properties ****/
@property (nonatomic, strong) NSTimer *timer;

/**** observers ****/
@end


@implementation FGSplashController
@synthesize timer;
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSLog(@"Splash: %f seconds.", FGDefaultSplashDuration);
	self.timer=[NSTimer scheduledTimerWithTimeInterval:FGDefaultSplashDuration target:self selector:@selector(handleTick:) userInfo:nil repeats:NO];
}

- (void)handleTick:(NSTimer*)timer
{
	[self.timer invalidate];
	[self.delegate modalComplete:self];
	[self dismissModalViewControllerAnimated:NO];
}

@end
