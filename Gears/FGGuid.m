//
//  FGGuid.m
//  Gears
//
//  Created by Curtis Elsasser on 3/27/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGuid.h"

@implementation FGGuid

+ (NSString*)create
{
	// create a new UUID which you own
	CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
	CFStringRef string=CFUUIDCreateString(kCFAllocatorDefault, uuid);
	NSString *result=[NSString stringWithCString:string encoding:NSUTF8StringEncoding];
	
	CFRelease(uuid);
	
	return result;
}

@end
