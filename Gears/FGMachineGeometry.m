//
//  FGMachineGeometry.m
//  Gears
//
//  Created by Curtis Elsasser on 3/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineGeometry.h"
#import "FGGearState.h"
#import "FGGear.h"
#import "FGDefaults.h"

static const float PI_2=M_PI*2;

@interface FGMachineGeometry()
/**** private interface ****/
+ (float)gearStateRotation:(FGGearState*)gearState;
@end

@implementation FGMachineGeometry

+ (CGRect)gearStateBounds:(FGGearState*)gearState parentBounds:(CGRect)parentBounds totalBounds:(CGRect)totalBounds
{
	float radiusRatio=[gearState.gear.radiusRatio floatValue];
	CGRect bounds=CGRectMake(parentBounds.origin.x, parentBounds.origin.y, totalBounds.size.width*radiusRatio, totalBounds.size.height*radiusRatio);
	
	if(gearState.parent!=nil)
	{
		float radiusChildOutter=bounds.size.width/2;
		float radiusParentOutter=parentBounds.size.width/2;
		float radiusParentInner=radiusParentOutter-FGDefaultGearRingWidth;
		float radiusToGearOrigin=radiusParentInner-radiusChildOutter;
		float circumferenceChildOutter=radiusChildOutter*PI_2;
		float circumferenceParentInner=radiusParentInner*PI_2;
		float rotationOffsetRadians=[self gearStateRotation:gearState.parent]*PI_2;
		float distanceTraveled=circumferenceChildOutter*gearState.rotationOffset;
		
		// add in the gears rotation
		rotationOffsetRadians-=(distanceTraveled/circumferenceParentInner)*PI_2;
		bounds.origin.x+=radiusParentOutter+(cosf(rotationOffsetRadians)*radiusToGearOrigin)-radiusChildOutter;
		bounds.origin.y+=radiusParentOutter+(sinf(rotationOffsetRadians)*radiusToGearOrigin)-radiusChildOutter;
	}
	
	return bounds;
}

/* Note: I could not figure out how to put a CGPoint in an NSArray.  NSValue has class methods: valueWithCGPoint, valueWithCGSize, etc. */
+ (void)gearStatePenPoints:(FGGearState*)gearState bounds:(CGRect)bounds points:(CGPoint[])points
{
	int penCount=[gearState.gear.penCount intValue];
	float originToPenRadius=bounds.size.width/2-FGDefaultGearRingWidth/2;
	float gearRotationOffsetRadians=[self gearStateRotation:gearState]*PI_2;
	
	for(int index=0; index<penCount; index++)
	{
		float penRotationOffsetRadians=gearRotationOffsetRadians+(((float)index/(float)penCount)*PI_2);
		points[index]=CGPointMake(CGRectGetMidX(bounds)+cosf(penRotationOffsetRadians)*originToPenRadius, CGRectGetMidY(bounds)+sinf(penRotationOffsetRadians)*originToPenRadius);
	}
}

+ (BOOL)canGearContainChild:(FGGear*)gear totalBounds:(CGRect)totalBounds
{
	CGFloat radiusRatio=[gear.radiusRatio floatValue];
	CGFloat outterBoundsParent=fminf(totalBounds.size.width*radiusRatio, totalBounds.size.height*radiusRatio);
	CGFloat outterBoundsChild=outterBoundsParent-FGDefaultGearRingWidth*2;
	
	return (outterBoundsChild>=FGDefaultGearRingWidth*2);
}

/**** private interface ****/
+ (float)gearStateRotation:(FGGearState*)gearState
{
	float result=gearState.rotationOffset;
	if(gearState.parent!=nil)
	{
		result+=[self gearStateRotation:gearState.parent];
	}
	return result;
}


@end
