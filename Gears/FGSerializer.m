//
//  FGSerialization.m
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGSerializer.h"
#import "NSNotification+FG.h"

static NSDateFormatter *dateFormatter;

@implementation FGSerializer

+ (void)initialize
{
	dateFormatter=[[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
}

#pragma mark - Utils
+ (NSString*)dateToString:(NSDate*)date
{
	return [dateFormatter stringFromDate:date];
}

+ (NSDate*)stringToDate:(NSString*)text
{
	return [dateFormatter dateFromString:text];
}

#pragma mark - Workers
+ (NSData*)dataToJSON:(id)object pretty:(BOOL)pretty
{
	NSData *json;
	NSError *error;
	@try 
	{
		NSJSONWritingOptions options=(pretty) ? NSJSONWritingPrettyPrinted : 0;
		json=[NSJSONSerialization dataWithJSONObject:object options:options error:&error];
		if(json==nil)
		{
			@throw [NSException exceptionWithName:@"SerializationException" reason:[error localizedDescription] userInfo:nil];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *text=[NSString stringWithFormat:@"Failed to serialize object to JSON: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
		// let 'em know
		@throw exception;
	}
	return json;
}

+ (id)jsonToData:(NSData*)json
{
	id data;
	NSError *error;
	@try 
	{
		data=[NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingAllowFragments error:&error];
		if(data==nil)
		{
			@throw [NSException exceptionWithName:@"SerializationException" reason:[error localizedDescription] userInfo:nil];
		}
	}
	@catch (NSException *exception) 
	{
		NSString *text=[NSString stringWithFormat:@"Failed to serialize object from JSON: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification warnNotificationWithObject:nil text:text]];
		// let 'em know
		@throw exception;
	}
	return data;

}

@end
