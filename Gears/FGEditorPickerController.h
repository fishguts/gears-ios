//
//  FGEditorPickerController.h
//  Gears
//
//  Created by Curtis Elsasser on 5/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@interface FGEditorPickerController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

/**** properties *****/
@property (nonatomic, copy) id identifier;

@property (nonatomic, copy) id selectedDataItem;
@property (nonatomic, weak) NSArray *dataSource;

@property (nonatomic) CGFloat dataSourceColumnWidth;
@property (nonatomic) UITextAlignment dataSourceColumnAlignment;

@property (nonatomic, weak) id<FGEditorDelegate> delegate;

- (int)selectedRow;
@end
