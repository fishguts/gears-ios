//
//  FGTextEditorTableViewCell.h
//  Gears
//
//  Created by Curtis Elsasser on 4/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGTextEditorTableViewCell : UITableViewCell <UITextFieldDelegate>
/**** properties ****/
@property (nonatomic, weak) IBOutlet UILabel *propertyName;
@property (nonatomic, weak) IBOutlet UITextField *propertyValue;
@end
