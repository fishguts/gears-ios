//
//  FGMachineFactoryController.h
//  Gears
//
//  Created by Curtis Elsasser on 5/3/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@class FGMachine;

@interface FGMachineFactoryController : UITableViewController <UITextFieldDelegate, FGEditorDelegate>

/**** properties ****/
@property (nonatomic) CGRect targetBounds;
@property (nonatomic, weak) id<FGModalDelegate> delegate;

/**** public interface ****/
- (FGMachine*)machineFromUI;

@end
