//
//  FGDrawingLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/21/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDrawingPathLayer.h"
#import "FGMachineState.h"
#import "FGGear.h"
#import "FGGearState.h"
#import "FGMachineGeometry.h"


@interface FGDrawingPathLayer()
{
@private
	NSMutableDictionary *_gearToPenPathsMap;
	NSMutableArray *_removedPenPaths;
}

/**** private interface ****/
- (void)drawPaths:(NSArray*)paths context:(CGContextRef)context;
@end

@implementation FGDrawingPathLayer

/**** public interface ****/
- (id)init
{
	self=[super init];
	if(self!=nil)
	{
		self->_gearToPenPathsMap=[NSMutableDictionary dictionary];
		self->_removedPenPaths=[NSMutableArray array];
	}
	return self;
}

- (void)dealloc
{
	[self clearLayer];
}

- (void)clearLayer
{
	NSValue *pathValue;
	CGMutablePathRef path;
	
	for(NSArray *paths in [self->_gearToPenPathsMap allValues])
	{
		for(pathValue in paths)
		{
			path=[pathValue pointerValue];
			CGPathRelease(path);
		}
	}
	for(pathValue in self->_removedPenPaths)
	{
		path=[pathValue pointerValue];
		CGPathRelease(path);
	}
	
	[self->_gearToPenPathsMap removeAllObjects];
	[self->_removedPenPaths removeAllObjects];
	[self setNeedsDisplay];
}

- (void)preMachineAdvance
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;

	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;

	while(gearState!=nil) 
	{
		CGMutablePathRef path;
		const int penCount=[gearState.gear.penCount intValue];
		NSMutableArray *paths=[self->_gearToPenPathsMap objectForKey:gearState.id];

		// make sure we've got the path array registered (just starting or new gear).
		if(paths==nil)
		{
			paths=[NSMutableArray array];
			[self->_gearToPenPathsMap setObject:paths forKey:gearState.id];
		}
		
		// note: must get each gear's bounds to find the next gears bounds.
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];

		// the following block creates inital points and removes no longer referenced pens
		// If we've already got the paths started then we are done for this gear.
		if([paths count]!=penCount)
		{
			if([paths count]<penCount)
			{
				CGPoint points[penCount];
				[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
				
				// and paths and points
				for(int penIndex=[paths count]; penIndex<penCount; penIndex++)
				{
					path=CGPathCreateMutable();
					[paths addObject:[NSValue valueWithPointer:path]];
					CGPathMoveToPoint(path, NULL, points[penIndex].x, points[penIndex].y);
				}
			}
			else
			{
				// points that have been removed
				while([paths count]>penCount)
				{
					[self->_removedPenPaths addObject:[paths lastObject]];
					[paths removeLastObject];
				}
			}
		}
		
		// advance
		gearState=gearState.child;
	}
}

- (void)postMachineAdvance
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	// we get the bounds of the topmost gear but he's not a drawer so we throw him away
	boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
	gearState=gearState.child;
	
	while(gearState!=nil)
	{
		int penCount=[gearState.gear.penCount intValue];

		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		if(penCount>0)
		{
			CGPoint points[penCount];
			NSMutableArray *paths=[self->_gearToPenPathsMap objectForKey:gearState.id];
			
			[FGMachineGeometry gearStatePenPoints:gearState bounds:boundsGear points:points];
			for(int penIndex=0; penIndex<penCount; penIndex++)
			{
				CGMutablePathRef path=[[paths objectAtIndex:penIndex] pointerValue];
				CGPathAddLineToPoint(path, NULL, points[penIndex].x, points[penIndex].y);
			}
			
		}
		// advance
		gearState=gearState.child;
	}
	
}


/**** drawing ****/
- (void)drawPaths:(NSArray *)paths context:(CGContextRef)context
{
	for(NSValue *pathValue in paths)
	{
		CGMutablePathRef path=[pathValue pointerValue];
		CGContextAddPath(context, path);
		CGContextStrokePath(context);
	}
}

- (void)drawInContext:(CGContextRef)context
{
	CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
	for(NSArray *penPaths in [self->_gearToPenPathsMap allValues])
	{
		[self drawPaths:penPaths context:context];
	}
	[self drawPaths:self->_removedPenPaths context:context];
}

@end
