//
//  FGGear+Math.m
//  Gears
//
//  Created by Curtis Elsasser on 4/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGear+Math.h"
#import "FGDefaults.h"

@implementation FGGear (Math)
- (float)outterCircumferenceWithTotalBounds:(CGSize)bounds
{
	float diameter=[self.radiusRatio floatValue]*fminf(bounds.width, bounds.height);
	return diameter*M_PI;
}

- (float)innerCircumferenceWithTotalBounds:(CGSize)bounds ringWidth:(float)ringWidth
{
	float diameter=[self.radiusRatio floatValue]*(fminf(bounds.width, bounds.height)-ringWidth*2);
	return diameter*M_PI;
}

- (float)radiusRatioMinWithTotalBounds:(CGSize)totalBounds
{
	float totalDiameter=fminf(totalBounds.width, totalBounds.height);
	if(self.child!=nil)
	{
		float childDiameter=[self.child.radiusRatio floatValue]*totalDiameter;
		return (childDiameter+FGDefaultGearRingWidth*2)/totalDiameter;
	}
	else
	{
		return (FGDefaultGearRingWidth*2)/totalDiameter;
	}	
}

- (float)radiusRatioMaxWithTotalBounds:(CGSize)totalBounds
{
	float totalDiameter=fminf(totalBounds.width, totalBounds.height);
	if(self.parent!=nil)
	{
		float parentDiameter=[self.parent.radiusRatio floatValue]*totalDiameter;
		return (parentDiameter-FGDefaultGearRingWidth*2)/totalDiameter;
	}
	else
	{
		return 1;
	}
}

- (float)childRadiusRatioMinWithTotalBounds:(CGSize)totalBounds
{
	float totalDiameter=fminf(totalBounds.width, totalBounds.height);
	return (FGDefaultGearRingWidth*2)/totalDiameter;
}

- (float)childRadiusRatioMaxWithTotalBounds:(CGSize)totalBounds
{
	float totalDiameter=fminf(totalBounds.width, totalBounds.height);
	float thisDiameter=[self.radiusRatio floatValue]*totalDiameter;
	return (thisDiameter-FGDefaultGearRingWidth*2)/totalDiameter;
}


@end
