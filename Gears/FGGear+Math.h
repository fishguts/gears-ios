//
//  FGGear+Math.h
//  Gears
//
//  Created by Curtis Elsasser on 4/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGear.h"

@interface FGGear (Math)
- (float)outterCircumferenceWithTotalBounds:(CGSize)bounds;
- (float)innerCircumferenceWithTotalBounds:(CGSize)bounds ringWidth:(float)ringWidth;

- (float)radiusRatioMinWithTotalBounds:(CGSize)totalBounds;
- (float)radiusRatioMaxWithTotalBounds:(CGSize)totalBounds;
- (float)childRadiusRatioMinWithTotalBounds:(CGSize)totalBounds;
- (float)childRadiusRatioMaxWithTotalBounds:(CGSize)totalBounds;


@end
