//
//  FGEditorPickerController.m
//  Gears
//
//  Created by Curtis Elsasser on 5/5/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGEditorPickerController.h"


@interface FGEditorPickerController()
/**** private properties ****/
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

/**** private interface *****/
- (void)selectedDataItemToUI;
@end

@implementation FGEditorPickerController
@synthesize pickerView=_pickerView;
@synthesize identifier=_identifier;
@synthesize selectedDataItem=_selectedDataItem;
@synthesize dataSource=_dataSource;
@synthesize dataSourceColumnWidth=_dataSourceColumnWidth;
@synthesize dataSourceColumnAlignment=_dataSourceColumnAlignment;
@synthesize delegate=_delegate;

#pragma mark - Properties
- (id)selectedDataItem
{
	if(self.pickerView!=nil)
	{
		int row=[self.pickerView selectedRowInComponent:0];
		return [self.dataSource objectAtIndex:row];
	}
	return self->_selectedDataItem;
}

- (void)setSelectedDataItem:(id)selectedDataItem
{
	if(selectedDataItem!=self->_selectedDataItem)
	{
		self->_selectedDataItem=selectedDataItem;
		[self selectedDataItemToUI];
	}
}

#pragma mark - Interface
- (int)selectedRow
{
	return [self.pickerView selectedRowInComponent:0];
}

- (void)selectedDataItemToUI
{
	if(self.pickerView!=nil)
	{
		const int row=[self.dataSource indexOfObject:self->_selectedDataItem];
		[self.pickerView selectRow:MAX(0, row) inComponent:0 animated:YES];
	}
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.pickerView.delegate=self;
	self.pickerView.dataSource=self;
	[self.pickerView reloadAllComponents];
	[self selectedDataItemToUI];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[self.delegate editorComplete:self];
}

#pragma mark - UIPickerViewDataSource and UIPickerViewDelegate

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
	return self.dataSourceColumnWidth;
}

- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *label=(UILabel*)view;
	if(label==nil)
	{
		CGSize rowSize=[self.pickerView rowSizeForComponent:component];
		
		label=[[UILabel alloc] init];
		label.textColor=[UIColor blackColor];
		label.backgroundColor=[UIColor clearColor];
		label.font=[UIFont boldSystemFontOfSize:20];
		label.textAlignment=self.dataSourceColumnAlignment;
		[label setBounds:CGRectMake(4, 0, rowSize.width-8, rowSize.height)];
	}
	label.text=[self pickerView:pickerView titleForRow:row forComponent:component];
	
	return label;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [self.dataSource count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [[self.dataSource objectAtIndex:row] description];
}

- (void)viewDidUnload {
	[self setTitle:nil];
	[super viewDidUnload];
}
@end
