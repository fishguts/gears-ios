//
//  FGMachineTableViewController.m
//  Gears
//
//  Created by Curtis Elsasser on 3/30/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachinesTableViewController.h"
#import "FGMachineFactoryController.h"
#import "FGModel.h"
#import "FGMachine.h"
#import "FGMachineFactory.h"
#import "FGNotifications.h"
#import "NSNotification+FG.h"
#import "FGDefaults.h"


@interface FGMachinesTableViewController()
@property (nonatomic, strong) NSFetchedResultsController *resultsController;

/**** private workers ****/
- (void)addObservers;
- (void)removeObservers;

- (void)showSplash;
- (void)buildFetchRequest;
- (void)configureCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath;

/**** observers ****/
- (void)handleDBIONotification:(NSNotification*)notification;
@end


@implementation FGMachinesTableViewController

#pragma mark - Properties
@synthesize resultsController=_resultsController;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"Machine Segue"])
	{
		if([sender isKindOfClass:[FGMachine class]])
		{
			[segue.destinationViewController performSelector:@selector(setMachine:) withObject:sender];
		}
		else if([sender isKindOfClass:[UITableViewCell class]])
		{
			NSIndexPath *indexPath=[self.tableView	indexPathForCell:sender];
			FGMachine *machine=[self.resultsController objectAtIndexPath:indexPath];
			[segue.destinationViewController performSelector:@selector(setMachine:) withObject:machine];
		}

		// make sure we are not in edit mode
		[self setEditing:NO];
	}
	else if([segue.identifier isEqualToString:@"Factory Segue"])
	{
		// todo: assuming this is in a navigation controller is pretty crappy.  Find a better way.
		FGMachineFactoryController *machineFactoryController=(FGMachineFactoryController*)((UINavigationController*)segue.destinationViewController).topViewController;
		machineFactoryController.delegate=self;
		// todo: how to generalize the target dimensions?  Right now they are embedded in the storyboard.
		machineFactoryController.targetBounds=CGRectMake(0, 0, 320, 320);
	}
}


#pragma mark - Private Interface
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	// db io notification
	[center addObserver:self selector:@selector(handleDBIONotification:) name:NotificationDBOpening object:nil];
	[center addObserver:self selector:@selector(handleDBIONotification:) name:NotificationDBOpenSucceeded object:nil];
	[center addObserver:self selector:@selector(handleDBIONotification:) name:NotificationDBOpenFailed object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showSplash
{
	if(FGDefaultSplashDuration>0)
	{
		UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Storyboard-iPhone" bundle:nil];
		UIViewController *splashController=[storyboard instantiateViewControllerWithIdentifier:@"SplashController"];
		[self presentViewController:splashController animated:NO completion:NULL];
	}
}

- (void)buildFetchRequest
{
	if(self.resultsController==nil)
	{
		NSError *error;

		FGModel *model=[FGModel instance];
		NSFetchRequest *request=[NSFetchRequest fetchRequestWithEntityName:@"FGMachine"];
		request.sortDescriptors=[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"modified" ascending:NO]];
		self.resultsController=[[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:model.context sectionNameKeyPath:nil cacheName:nil];
		self.resultsController.delegate=self;
		
		if([self.resultsController performFetch:&error]==YES)
		{
			// this doesn't seem that it should be needed but view controller does not automatically reload the view.
			[self.tableView reloadData];
		}
		else
		{
			NSString *text=[NSString stringWithFormat:@"Failed to load database: %@", [error localizedDescription]];
			[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:text]];
		}
	}
}

- (void)configureCell:(UITableViewCell*)cell indexPath:(NSIndexPath*)indexPath
{
	FGMachine *machine=[self.resultsController objectAtIndexPath:indexPath];
	cell.textLabel.text=machine.title;
	cell.imageView.image=[UIImage imageWithData:machine.thumb];
	// for some reason (at least in emulator) the image does not show up after return from new machine.
	[cell setNeedsLayout];
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self addObservers];
	[[FGModel instance] open];
	
	// configure UI
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	
	// start us up
	[self showSplash];
}

- (void)viewDidUnload
{
	[self removeObservers];
	[super viewDidUnload];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return MAX(1, [[self.resultsController sections] count]);
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    NSInteger rowCount=0;
    if([[self.resultsController sections] count] > 0) 
	{
        id <NSFetchedResultsSectionInfo> sectionInfo=[[self.resultsController sections] objectAtIndex:section];
        rowCount = [sectionInfo numberOfObjects];
    }
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Machine Cell"];
	[self configureCell:cell indexPath:indexPath];
	
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if(editingStyle==UITableViewCellEditingStyleDelete) 
	{
		// note: this edit will be picked by our UITableViewResultsController
		FGMachine *machine=[self.resultsController objectAtIndexPath:indexPath];
		[FGMachineFactory deleteMachine:machine];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller 
{
	// The fetch controller is about to start sending change notifications, so prepare the table view for updates.
	[self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller 
{
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
	[self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
	switch(type)
	{
		case NSFetchedResultsChangeInsert:
			[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] indexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
	}
}

#pragma mark - FGModalDelegate
- (void)modalCanceled:(id)modal
{
	[modal dismissModalViewControllerAnimated:YES];
}

- (void)modalComplete:(id)modal
{
	FGMachine *machine=[modal machineFromUI];
	[modal dismissViewControllerAnimated:NO completion:^(void)
	 {
		 [self performSegueWithIdentifier:@"Machine Segue" sender:machine];
	 }];
}


#pragma mark - Handlers
- (IBAction)handleAddGear:(id)sender 
{
#if FREE_BUILD
	FGMachine *machine=[FGMachineFactory createMachine];
	[self performSegueWithIdentifier:@"Machine Segue" sender:machine];
#else
	[self performSegueWithIdentifier:@"Factory Segue" sender:self];
#endif
}


- (void)handleDBIONotification:(NSNotification*)notification
{
	// open
	if([notification.name isEqualToString:NotificationDBOpening])
	{
	}
	else if([notification.name isEqualToString:NotificationDBOpenSucceeded])
	{
		[self buildFetchRequest];
	}
	else if([notification.name isEqualToString:NotificationDBOpenFailed])
	{	
	}
	
}

@end
