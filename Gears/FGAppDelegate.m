//
//  FGAppDelegate.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import "FGAppDelegate.h"
#import "FGMachineFactory.h"
#import "FGNotifications.h"
#import "NSNotification+FG.h"
#import "FGTests.h"


@interface FGAppDelegate()
/**** private workers ****/
- (void)addObservers;
- (void)removeObservers;
- (void)validatePresets;

/**** observers ****/
- (void)handleAlertNotification:(NSNotification*)notification;

@end



@implementation FGAppDelegate

@synthesize window = _window;

- (id)init
{
	NSLog(@"Version=%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]);
	[FGTests runSimple];

	self=[super init];
	[self addObservers];

	return self;
}

/**** private workers ****/
- (void)addObservers
{
	NSNotificationCenter *center=[NSNotificationCenter defaultCenter];
	
	// alerts: error, warn, info, debug
	[center addObserver:self selector:@selector(handleAlertNotification:) name:NotificationError object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:NotificationWarn object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:NotificationInfo object:nil];
	[center addObserver:self selector:@selector(handleAlertNotification:) name:NotificationDebug object:nil];
	[center addObserver:self selector:@selector(handleDBOpenSucceeded:) name:NotificationDBOpenSucceeded object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)validatePresets
{
	@try 
	{
		if([[NSUserDefaults standardUserDefaults] boolForKey:@"presetsInstalled"]==NO)
		{
			// update default in the event that there is a problem - it won't come up each time.
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"presetsInstalled"];
			[[NSUserDefaults standardUserDefaults] synchronize];
			[FGMachineFactory addPresets];
		}

	}
	@catch (NSException *exception) 
	{
		// let them know so that we get feedback if it happens
		NSString *text=[NSString stringWithFormat:@"Error adding presets: %@", [exception description]];
		[[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:text]];
	}
}


/**** private notification handlers *****/
- (void)handleAlertNotification:(NSNotification*)notification
{
	if([notification.name isEqualToString:NotificationError])
	{
		NSLog(@"Error: %@", [notification alertText]);
		UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:[notification alertText] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
	}
	else if([notification.name isEqualToString:NotificationWarn])
	{
		NSLog(@"Warn: %@", [notification alertText]);
	}
	else if([notification.name isEqualToString:NotificationInfo])
	{
		NSLog(@"Info: %@", [notification alertText]);
	}
	else if([notification.name isEqualToString:NotificationDebug])
	{
		NSLog(@"Debug: %@", [notification alertText]);
	}
}

- (void)handleDBOpenSucceeded:(NSNotification*)notification
{
	[FGTests runDB];
	[self validatePresets];
}

#pragma mark - lifecycle handlers
- (void)applicationWillResignActive:(UIApplication *)application
{
	NSLog(@"applicationWillResignActive");
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	NSLog(@"applicationDidEnterBackground");
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	NSLog(@"applicationWillEnterForeground");
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	NSLog(@"applicationDidBecomeActive");
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	NSLog(@"applicationWillTerminate");
	[self removeObservers];
	// todo - to save here or not?

	/*
	 Called when the application is about to terminate.
	 Save data if appropriate.
	 See also applicationDidEnterBackground:.
	 */
}

@end
