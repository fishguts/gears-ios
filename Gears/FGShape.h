//
//  FGShape.h
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

/* builds shapes that are expressed as percentages of bounds */
@interface FGShape : FGStaticClass

+ (void)buildShape:(const CGPoint[])points length:(int)length bounds:(CGRect)bounds context:(CGContextRef)context;

+ (void)buildDoubleHeadedArrowHorizontal:(CGRect)bounds context:(CGContextRef)context;
+ (void)buildDoubleHeadedArrowVertical:(CGRect)bounds context:(CGContextRef)context;
@end
