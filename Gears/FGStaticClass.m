//
//  FGStaticClass.m
//  Gears
//
//  Created by Curtis Elsasser on 3/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@implementation FGStaticClass
- (id)init
{
	@throw [NSException exceptionWithName:@"StaticClassException" reason:@"Don't instantiate" userInfo:nil];
}
@end
