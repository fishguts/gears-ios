//
//  FGInfoLayer.h
//  Gears
//
//  Created by Curtis Elsasser on 3/26/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBaseLayer.h"
#import "FGTypes.h"

@class FGGear;

@interface FGInfoLayer : FGBaseLayer
/**** properties ****/
@property (nonatomic) FGInfoOverlayType overlayState;
@property (nonatomic, weak) FGGear *gear;

@end
