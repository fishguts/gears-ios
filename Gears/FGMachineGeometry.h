//
//  FGMachineGeometry.h
//  Gears
//
//  Created by Curtis Elsasser on 3/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@class FGGear;
@class FGGearState;

@interface FGMachineGeometry : FGStaticClass

/**** public interface ****/
+ (CGRect)gearStateBounds:(FGGearState*)gearState parentBounds:(CGRect)parentBounds totalBounds:(CGRect)totalBounds;
+ (void)gearStatePenPoints:(FGGearState*)gearState bounds:(CGRect)bounds points:(CGPoint[])points;

+ (BOOL)canGearContainChild:(FGGear*)gear totalBounds:(CGRect)totalBounds;

@end
