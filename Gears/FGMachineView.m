//
//  FGMachineView.m
//  Gears
//
//  Created by Curtis Elsasser on 3/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineView.h"
#import "FGDrawingBitmapLayer.h"
#import "FGDrawingFadeLayer.h"
#import "FGGearsLayer.h"
#import "FGInfoLayer.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGGearState.h"
#import "FGDefaults.h"
#import <QuartzCore/QuartzCore.h>


@interface FGMachineView()
/**** properties ****/
@property (nonatomic, strong) FGDrawingLayer *layerDrawing;
@property (nonatomic, strong) FGGearsLayer *layerGears;
@property (nonatomic, strong) FGInfoLayer *layerInfo;

/**** utilities ****/
+ (void)animateLayer:(CALayer*)layer opacityFrom:(float)opacityFrom opacityTo:(float)opacityTo duration:(NSTimeInterval)duration delegate:(id)delegate;

/**** private interface ****/
- (void)setupLayers;
@end


@implementation FGMachineView

#pragma mark - Properties
@synthesize machineState=_machineState;
@synthesize layerDrawing=_layerDrawing;
@synthesize layerGears=_layerGears;
@synthesize layerInfo=_layerInfo;


- (void)setMachineState:(FGMachineState *)machineState
{
	if(_machineState!=machineState)
	{
		_machineState=machineState;
		[self setupLayers];
		[self redrawMachineState];
	}
}

- (CGRect)topLevelGearBounds
{
	return [self.layerGears topLevelGearBounds];
}

#pragma mark - Public interface
- (FGGearState*)gearStateFromPoint:(CGPoint)point
{
	return [self.layerGears gearStateFromPoint:point];
}

- (FGGearState*)gearStateFromPoint:(CGPoint)point withRadius:(CGFloat)radius
{
	return [self.layerGears gearStateFromPoint:point withRadius:radius];
}


- (void)clearDrawing
{
	[self.layerDrawing clearLayer];
}

- (void)validateDrawingMethod
{
	FGDrawingLayer *oldLayer=self.layerDrawing;
	if(self.machineState.machine.drawingFadeDelay!=nil)
	{
		if([self.layerDrawing isKindOfClass:[FGDrawingFadeLayer class]]==NO)
		{
			self.layerDrawing=[[FGDrawingFadeLayer alloc] initWithFrame:self.bounds];
		}
	}
	else
	{
		if([self.layerDrawing isKindOfClass:[FGDrawingBitmapLayer class]]==NO)
		{
			self.layerDrawing=[[FGDrawingBitmapLayer alloc] initWithFrame:self.bounds];
		}
	}
	// update if we switched methods
	if(oldLayer!=self.layerDrawing)
	{
		self.layerDrawing.machineState=self.machineState;
		[self.layer replaceSublayer:oldLayer with:self.layerDrawing];
	}
}


- (void)preMachineAdvance
{
	[self.layerDrawing preMachineAdvance];
}

- (void)postMachineAdvance
{
	[self.layerDrawing postMachineAdvance];
}

- (void)redrawMachineState
{
	[self.layerDrawing setNeedsDisplay];
	[self.layerGears setNeedsDisplay];
}

- (UIImage*)imageFromGears:(CGSize)size
{
	UIGraphicsBeginImageContextWithOptions(size, YES, 1.0);
	{
		CGContextRef context=UIGraphicsGetCurrentContext();

		// scale so that we can treat this guy like he's on our own surface which the layers are going to think anyway.
		float scale=MIN(size.width, size.height)/self.bounds.size.width;
		CGContextScaleCTM(context, scale, scale);
		
		// background
		CGContextSetFillColorWithColor(context, [[UIColor whiteColor] CGColor]);
		CGContextFillRect(context, CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height));
		
		// gears
		[self.layerGears drawInContext:context];
	}
	// in that we created this guy, I think it's okay that we use him directly.
	return UIGraphicsGetImageFromCurrentImageContext();
}

- (UIImage*)imageFromDrawing:(CGSize)size
{
	return nil;
}

/**** public interface: info layer ****/
- (void)showInfoLayerWithGear:(FGGearState*)gearState infoType:(FGInfoOverlayType)type pulse:(BOOL)pulse
{
	// create a new layer which will be the active layer for info.  If there is currently another he should be on his way out via either a pulse animation or hide animation.
	self.layerInfo=[[FGInfoLayer alloc] initWithFrame:self.bounds];
	self.layerInfo.gear=gearState.gear;
	self.layerInfo.overlayState=type;
	[self.layer addSublayer:self.layerInfo];
		 
	// using transaction to prevent implicit animations (actions) from conflicting with our explicit animations
	[CATransaction begin];
	[CATransaction setDisableActions:YES];
	{
		if(pulse)
		{
			// setup animation
			CAKeyframeAnimation *animation=[CAKeyframeAnimation animationWithKeyPath:@"opacity"];
			animation.values=[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:1.0], [NSNumber numberWithFloat:1.0], [NSNumber numberWithFloat:0.0], nil];
			animation.keyTimes=[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.15], [NSNumber numberWithFloat:0.65], [NSNumber numberWithFloat:1.0], nil];
			animation.timingFunctions=[NSArray arrayWithObjects:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn], nil];
			animation.duration=1.5;
			animation.delegate=self;
			self.layerInfo.opacity=0;
			[self.layerInfo addAnimation:animation forKey:@"opacity"];
		}
		else
		{
			[FGMachineView animateLayer:self.layerInfo opacityFrom:0.0 opacityTo:1.0 duration:0.15 delegate:nil];
		}
	}
	[CATransaction commit];
}

- (void)updateInfoLayer
{
	[self.layerInfo setNeedsDisplay];
}

- (void)hideInfoLayer:(BOOL)animate
{
	const float duration=(animate) ? 0.35 : 0;
	[FGMachineView animateLayer:self.layerInfo opacityFrom:self.layerInfo.opacity opacityTo:0.0 duration:duration delegate:self];
}

- (void)showGearLayer:(BOOL)animate
{
	const float duration=(animate) ? 0.35 : 0;
	[FGMachineView animateLayer:self.layerGears opacityFrom:self.layerGears.opacity opacityTo:1.0 duration:duration delegate:nil];
}

- (void)hideGearLayer:(BOOL)animate
{
	const float duration=(animate) ? 0.35 : 0;
	[FGMachineView animateLayer:self.layerGears opacityFrom:self.layerGears.opacity opacityTo:0.0 duration:duration delegate:nil];
}

/**** utilities ****/
+ (void)animateLayer:(CALayer*)layer  opacityFrom:(float)opacityFrom opacityTo:(float)opacityTo duration:(NSTimeInterval)duration delegate:(id)delegate
{
	[CATransaction begin];
	[CATransaction setDisableActions:YES];
	{
		CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"opacity"];
		animation.fromValue=[NSNumber numberWithFloat:opacityFrom];
		layer.opacity=opacityTo;
		animation.duration=duration;
		animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
		animation.delegate=delegate;
		[layer addAnimation:animation forKey:@"opacity"];
	}
	[CATransaction commit];
}


/**** private interface ****/
- (void)setupLayers
{
	if([self.layer sublayers]==nil)
	{
		self.layerDrawing=(self.machineState.machine.drawingFadeDelay!=nil) 
			? [[FGDrawingFadeLayer alloc] initWithFrame:self.bounds] 
			: [[FGDrawingBitmapLayer alloc] initWithFrame:self.bounds];
		[self.layer addSublayer:self.layerDrawing];

		// add a layer for the gears
		self.layerGears=[[FGGearsLayer alloc] initWithFrame:self.bounds];
		[self.layer addSublayer:self.layerGears];
	}
	
	self.layerDrawing.machineState=self.machineState;
	self.layerGears.machineState=self.machineState;
}

#pragma mark - CAAnimation delegates
- (void)animationDidStart:(CAAnimation *)anim
{
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
	// there may be other layers in the queue, nonetheless the retired
	// layer should be this fella.
	CALayer *layer=[[self.layer sublayers] objectAtIndex:2];
	[layer removeFromSuperlayer];
	if(layer==self.layerInfo)
	{
		self.layerInfo=nil;
	}
}


@end
