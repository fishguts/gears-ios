//
//  FGDrawingPath.m
//  Gears
//
//  Created by Curtis Elsasser on 5/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGDrawingPath.h"

static const int POINT_BLOCK=150;

@interface FGDrawingPath()
{
@private
	CGPoint *_points;
	int _pointsLength;
	int _pointsBegin;
	int _pointsEnd;
}

- (void)resizePointBuffer:(int)size;

@end

@implementation FGDrawingPath
@synthesize frameStart;
@synthesize frameLastPoint;


- (id)init
{
	return [self initWithFrameStart:0];
}

- (void)dealloc
{
	free(self->_points);
	self->_points=NULL;
}

- (id)initWithFrameStart:(int)frame
{
	self=[super init];
	self.frameStart=frame;
	[self resizePointBuffer:POINT_BLOCK];
	
	return self;
}

- (void)addPoint:(CGPoint)point frame:(int)frame
{
	if(self->_pointsEnd>=self->_pointsLength)
	{
		// beginning >= 30: don't let add and remove chase one another.  Always make sure we've got some room to breath.
		if(self->_pointsBegin>=30)
		{
			memcpy(self->_points, self->_points+self->_pointsBegin, (self->_pointsEnd-self->_pointsBegin)*sizeof(CGPoint));
			self->_pointsEnd-=self->_pointsBegin;
			self->_pointsBegin=0;
		}
		else
		{
			[self resizePointBuffer:self->_pointsLength+POINT_BLOCK];
		}
	}
	self->_points[self->_pointsEnd++]=point;
	self.frameLastPoint=frame;
}

- (int)getPointCount
{
	return self->_pointsEnd-self->_pointsBegin;
}

- (CGPoint)getPointAt:(int)index
{
	return self->_points[self->_pointsBegin+index];
}

- (CGPoint*)getPointBufferAt:(int)index
{
	return self->_points+(self->_pointsBegin+index);
}

- (void)removePointsFromBeginning:(int)count
{
	self->_pointsBegin+=count;
}


/**** private interface ****/
- (void)resizePointBuffer:(int)size
{
	CGPoint *buffer=malloc(size*sizeof(CGPoint));
	if(self->_points!=NULL)
	{
		memcpy(buffer, self->_points+self->_pointsBegin, (self->_pointsEnd-self->_pointsBegin)*sizeof(CGPoint));
		self->_pointsEnd-=self->_pointsBegin;
		self->_pointsBegin=0;
		free(self->_points);
	}
	self->_points=buffer;
	self->_pointsLength=size;
}

@end
