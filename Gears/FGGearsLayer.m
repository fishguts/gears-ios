//
//  FGearsLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/15/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGGearsLayer.h"
#import "FGMachineState.h"
#import "FGGearState.h"
#import "FGMachineGeometry.h"
#import "FGGear.h"
#import "FGDefaults.h"

@interface FGGearsLayer()
/**** private interface ****/
- (void)drawGearState:(FGGearState*)gearState bounds:(CGRect)bounds context:(CGContextRef)context;

@end


@implementation FGGearsLayer

/**** properties ****/
@synthesize machineState=_machineState;

- (void)setMachineState:(FGMachineState *)machineState
{
	if(machineState!=_machineState)
	{
		_machineState=machineState;
		[self setNeedsDisplay];
	}
}

/**** public interface ****/
- (FGGearState*)gearStateFromPoint:(CGPoint)point
{
	return [self gearStateFromPoint:point withRadius:0.5];
}

- (FGGearState*)gearStateFromPoint:(CGPoint)point withRadius:(CGFloat)radius
{
	FGGearState *gearState=self.machineState.topLevelGearState;
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	
	while(gearState!=nil)
	{
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		// quick check to make sure we are in the ballpark
		if(CGRectContainsPoint(boundsGear, point))
		{
			const float distance=sqrtf(powf(CGRectGetMidX(boundsGear)-point.x, 2)+powf(CGRectGetMidY(boundsGear)-point.y, 2));
			const float radiusOutter=boundsGear.size.width/2;
			if(distance<=(radiusOutter+radius))
			{
				const float radiusInner=radiusOutter-FGDefaultGearRingWidth;
				if(distance>=(radiusInner-radius))
				{
					break;
				}
			}
			
			gearState=gearState.child;
		}
		else
		{
			// we are totally out of bounds
			gearState=nil;
		}
	}
	
	return gearState;
}


/**** private interface ****/
- (void)drawGearState:(FGGearState*)gearState bounds:(CGRect)bounds context:(CGContextRef)context
{
	int penCount=[gearState.gear.penCount intValue];
	
	// draw gear body
	CGContextSetStrokeColorWithColor(context, [[UIColor blackColor] CGColor]);
	CGContextSetFillColorWithColor(context, [(gearState.selected) ? [FGDefaults getGearColorSelected] : [FGDefaults getGearColorNormal] CGColor]);
	
	CGContextAddEllipseInRect(context, bounds);
	CGContextAddEllipseInRect(context, CGRectInset(bounds, FGDefaultGearRingWidth, FGDefaultGearRingWidth));
	
	CGContextDrawPath(context, kCGPathEOFillStroke);
	
	// draw pen points
	if(penCount>0)
	{
		CGPoint points[penCount];

		[FGMachineGeometry gearStatePenPoints:gearState bounds:bounds points:points];
		CGContextSetFillColorWithColor(context, [[UIColor blackColor] CGColor]);
		for(int index=0; index<penCount; index++)
		{
			bounds=CGRectMake(points[index].x-FGDefaultPenRadius, points[index].y-FGDefaultPenRadius, FGDefaultPenRadius*2, FGDefaultPenRadius*2);
			CGContextFillEllipseInRect(context, bounds);
		}
	}
}

/**** overrides ****/
- (void)drawInContext:(CGContextRef)context
{
	CGRect boundsTotal=self.topLevelGearBounds;
	CGRect boundsGear=boundsTotal;
	FGGearState *gearState=self.machineState.topLevelGearState;
	
	while(gearState!=nil)
	{
		boundsGear=[FGMachineGeometry gearStateBounds:gearState parentBounds:boundsGear totalBounds:boundsTotal];
		[self drawGearState:gearState bounds:boundsGear context:context];
		
		gearState=gearState.child;
	}
}

@end
