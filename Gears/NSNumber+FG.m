//
//  NSNumber+FG.m
//  Gears
//
//  Created by Curtis Elsasser on 5/9/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "NSNumber+FG.h"

@implementation NSNumber (FG)

+ (BOOL)isEqualNumber:(NSNumber*)n1 toNumber:(NSNumber*)n2
{
	if(n1==n2)
	{
		return YES;
	}
	else if(n1==nil)
	{
		return NO;
	}
	else if(n2==nil)
	{
		return NO;
	}
	return [n1 isEqualToNumber:n2];
}


@end
