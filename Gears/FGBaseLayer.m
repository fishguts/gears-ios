//
//  FGBaseLayer.m
//  Gears
//
//  Created by Curtis Elsasser on 3/27/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGBaseLayer.h"

@implementation FGBaseLayer

- (id)initWithFrame:(CGRect)frame
{
	// being the designated guy make sure we call him on ourselves in the event that one of our layers overrides him
	self=[self init];
	self.frame=frame;
	return self;
}

- (CGRect)topLevelGearBounds
{
	// inset so that the lines don't get clipped (the view is extended by 1 on all sides).
	return CGRectInset(self.bounds, 1, 1);
}


@end
