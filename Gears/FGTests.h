//
//  FGTest.h
//  Gears
//
//  Created by Curtis Elsasser on 6/20/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGStaticClass.h"

@interface FGTests : FGStaticClass

+ (void)runSimple;
+ (void)runDB;

@end
