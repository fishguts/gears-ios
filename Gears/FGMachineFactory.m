//
//  FGMachineFactory.m
//  Gears
//
//  Created by Curtis Elsasser on 3/11/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachineFactory.h"
#import "FGModel.h"
#import "FGMachine.h"
#import "FGMachineState.h"
#import "FGMachineSerializer.h"
#import "FGGear.h"
#import "FGGearState.h"
#import "FGDefaults.h"
#import "FGNotifications.h"
#import "NSNotification+FG.h"

static NSString *presetJSON=@"["
"{'title':'Preset 1','gearTopLevel':{'child':{'child':{'child':{'penCount':2,'rpm':0,'radiusRatio':0.1532865},'penCount':3,'rpm':0,'radiusRatio':0.306573},'penCount':2,'rpm':40,'radiusRatio':0.5},'penCount':0,'rpm':0,'radiusRatio':1},'modified':'1/1/12 12:02 PM','created':'1/1/12 12:00 PM'}"
",{'drawingFadeDuration':1,'title':'Preset 2','drawingFadeDelay':11,'modified':'1/1/12 12:01 PM','gearTopLevel':{'child':{'child':{'child':{'child':{'penCount':20,'rpm':-15,'radiusRatio':0.2718784},'penCount':0,'rpm':10,'radiusRatio':0.6551693},'penCount':0,'rpm':-22,'radiusRatio':0.7498878},'penCount':0,'rpm':10,'radiusRatio':0.8585773},'penCount':0,'rpm':0,'radiusRatio':1},'created':'1/1/12 12:00 PM'}"
",{'title':'Preset 3','gearTopLevel':{'child':{'child':{'penCount':2,'rpm':10,'radiusRatio':0.1334912},'penCount':0,'rpm':38,'radiusRatio':0.2669823},'penCount':0,'rpm':0,'radiusRatio':1},'modified':'1/1/12 12:00 PM','created':'1/1/12 12:00 PM'}"
// ",{'title':'Preset 3','gearTopLevel':{'child':{'child':{'penCount':2,'rpm':10,'radiusRatio':0.1334912},'penCount':0,'rpm':36,'radiusRatio':0.4037993},'penCount':0,'rpm':0,'radiusRatio':1},'modified':'1/1/12 12:00 PM','created':'1/1/12 12:00 PM'}"
"]";


@implementation FGMachineFactory

/**** public interface ****/
+ (FGMachine*)createMachine
{
	FGMachine *machine=[self createMachineWithTitle:@"Untitled"];
	[FGMachineFactory createGearWithParent:machine.gearTopLevel machine:machine];
	return machine;
}

+ (FGMachine*)createMachineWithTitle:(NSString*)title
{
	FGModel *model=[FGModel instance];
	NSManagedObjectContext *context=[model context];
	FGMachine *machine=[NSEntityDescription insertNewObjectForEntityForName:@"FGMachine" inManagedObjectContext:context];
	
	machine.title=title;
	machine.created=[NSDate date];
	machine.modified=[NSDate date];

	// send notification before we add the toplevel gear so that the sequence of these notifications make sense
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification databaseModifiedWithObject:machine autoSave:YES]];
	
	machine.gearTopLevel=[self createGearWithParent:nil radiusRatio:1.0 penCount:0 rpm:0 machine:machine];

	return machine;
}

+ (void)deleteMachine:(FGMachine*)machine
{
	FGModel *model=[FGModel instance];
	NSManagedObjectContext *context=[model context];

	[context deleteObject:machine];
	// note: autosaving adds and removes
	[[NSNotificationCenter defaultCenter] postNotification:[NSNotification databaseModifiedWithObject:machine autoSave:YES]];
}

+ (FGMachineState*)createMachineState:(FGMachine*)machine
{
	FGMachineState *machineState=[[FGMachineState alloc] initWithMachine:machine];
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationMachineStateChanged object:machineState];
	
	return machineState;
}


+ (FGGear*)createGearWithParent:(FGGear*)parent machine:(FGMachine*)machine
{
	float radiusRatio=[parent.radiusRatio floatValue]/2;
	return [self createGearWithParent:parent radiusRatio:radiusRatio penCount:FGDefaultGearPenCount rpm:FGDefaultGearRPM machine:machine];
}

+ (FGGear*)createGearWithParent:(FGGear*)parent radiusRatio:(float)radiusRatio penCount:(int)penCount rpm:(float)rpm machine:(FGMachine*)machine
{
	NSManagedObjectContext *context=[[FGModel instance] context];
	FGGear *gear=[NSEntityDescription insertNewObjectForEntityForName:@"FGGear" inManagedObjectContext:context];
	
	parent.child=gear;
	gear.parent=parent;
	gear.radiusRatio=[NSNumber numberWithFloat:radiusRatio];
	gear.penCount=[NSNumber numberWithInt:penCount];
	gear.rpm=[NSNumber numberWithFloat:rpm];
	machine.modified=[NSDate date];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBModified object:gear];
	
	return gear;
}

+ (FGGearState*)createGearStateWithParent:(FGGearState*)parent machine:(FGMachineState*)machineState;
{
	FGGear *gear=[self createGearWithParent:parent.gear machine:machineState.machine];
	FGGearState *gearState=[[FGGearState alloc] initWithGear:gear parent:parent];
	
	// start the rotation at the offset he would be at as if 
	// he was always there.
	gearState.rotationOffset=[gearState.gear.rpm floatValue] * machineState.timeOffset/60;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationMachineStateChanged object:gearState];
	
	return gearState;
}

+ (void)addPresets
{
	// parser does not like single quotes.
	NSString *json=[presetJSON stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
	[FGMachineSerializer jsonToMachines:json addToDB:YES];
}


@end
