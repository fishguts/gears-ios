//
//  FGMachineTableViewController.h
//  Gears
//
//  Created by Curtis Elsasser on 4/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGTypes.h"

@class FGMachine;

@interface FGMachineTableViewController : UITableViewController <FGEditorDelegate>
{
@private
	float _fadeDelay;
	float _fadeDuration;

	NSMutableArray *_editingFriendlyValues;
	NSMutableArray *_editingDataValues;
}

@property (nonatomic, weak) FGMachine *machine;
@property (nonatomic, weak) id <FGEditorDelegate> delegate;
@end
