//
//  FGModel.m
//  Gears
//
//  Created by Curtis Elsasser on 3/9/12.
//  Copyright (c) 2012 Curtis Elsasser Inc. All rights reserved.
//

#import "FGModel.h"
#import "FGNotifications.h"
#import "NSNotification+FG.h"

#import <CoreData/CoreData.h>


static FGModel *instance;

/**** private api ****/
@interface FGModel()
@property (nonatomic, strong) UIManagedDocument *document;

- (void)addObservers;
- (void)removeObservers;
@end


/**** implementation ****/
@implementation FGModel

#pragma mark - Properties
@synthesize document=_document;

- (NSManagedObjectContext*)context
{
	return self.document.managedObjectContext;
}

- (UIManagedDocument*)document
{
	if(_document==nil)
	{
		NSURL *url=[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
		url=[url URLByAppendingPathComponent:@"GearDatabase"];
		_document=[[UIManagedDocument alloc] initWithFileURL:url];
	}
	return _document;
}


#pragma mark - Singleton and initialization
+ (void)initialize
{
	NSAssert(instance==nil, @"Should be nil");
	instance=[[FGModel alloc] init];
}

+ (FGModel*)instance
{
	NSAssert(instance!=nil, @"Should not be nil");
	return instance;
}

- (id)init
{
	self=[super init];
	[self addObservers];
	
	return self;
}


#pragma mark - Observer management and handling
- (void)addObservers
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDBModified:) name:NotificationDBModified object:nil];
}

- (void)removeObservers
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}
	 
- (void)handleDBModified:(NSNotification*)notification
{
	// this will let the document know that it's in a dirty state
	[self.document updateChangeCount:UIDocumentChangeDone];
	if([notification isDatabaseAutoSave])
	{
		[self save];
	}

}

#pragma mark - DB io
- (void)open
{
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpening object:self];
	if(self.document.documentState==UIDocumentStateNormal)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpenSucceeded object:self];
	}
	else
	{
		NSFileManager *manager=[NSFileManager defaultManager];
		if([manager fileExistsAtPath:[self.document.fileURL path]]==NO)
		{
			[self.document saveToURL:self.document.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success)
			 {
				 if(success)
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpenSucceeded object:self];
				 }
				 else
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpenFailed object:self];
					 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to open database"]];
				 }
				 
			 }];
		}
		else
		{
			[self.document openWithCompletionHandler:^(BOOL success)
			 {
				 if(success)
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpenSucceeded object:self];
				 }
				 else
				 {
					 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBOpenFailed object:self];
					 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to open database"]];
				 }
			 }];
		}
	}
}

- (void)save
{
	[[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBSaving object:self];
	[self.document saveToURL:self.document.fileURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL success)
	 {
		 if(success)
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBSaveSucceeded object:self];
		 }
		 else
		 {
			 [[NSNotificationCenter defaultCenter] postNotificationName:NotificationDBSaveFailed object:self];
			 [[NSNotificationCenter defaultCenter] postNotification:[NSNotification errorNotificationWithObject:self text:@"Failed to save database"]];
		 }
	 }];

}
@end
