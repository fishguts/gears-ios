//
//  FGMachine.m
//  Gears
//
//  Created by Curtis Elsasser on 5/4/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import "FGMachine.h"
#import "FGGear.h"


@implementation FGMachine

@dynamic author;
@dynamic created;
@dynamic modified;
@dynamic thumb;
@dynamic title;
@dynamic drawingFadeDuration;
@dynamic drawingFadeDelay;
@dynamic gearTopLevel;

@end
