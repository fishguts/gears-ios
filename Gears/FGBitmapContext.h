//
//  FGBitmapContext.h
//  BitmapCaching
//
//  Created by Curtis Elsasser on 6/17/12.
//  Copyright (c) 2012 Curtis Elsasser Incorporated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGBitmapContext : NSObject
{
@private
	CGContextRef _context;
	CGSize _bitmapSize;
	int _bitsPerComponent;
	int _bitsPerPixel;
	int _bytesPerRow;
	void *_buffer;
	CGBitmapInfo _bitmapInfo;
	
	CGColorSpaceRef _colorSpace;
	CGDataProviderRef _provider;
	
}

/**** properties ****/
@property (nonatomic, readonly) CGContextRef context;

/**** initialization and destruction ****/
- (id)initWithSize:(CGSize)size;
- (void)dispose;

// must release image once done with reference
- (CGImageRef)getSnapshot:(BOOL)deepCopy;
@end
